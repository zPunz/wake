import torch
import torch.nn as nn
import torch.nn.functional as F 

import numpy as np
import pdb




class Model(nn.Module):
    def __init__(self):
        super(Model, self).__init__()
        self.tuan = nn.Linear(34,0)
    def forward(self, input, target):
        input = input.transpose(1,2).contiguous()
        target = target.view([target.shape[0],target.shape[1],-1]).transpose(1,2).contiguous()
        pred = target
        loss = None
        nor_crosscor = None
        if target is not None:
            loss = F.mse_loss(pred,target[:,:,-pred.shape[2]:],reduction='none')
            N = pred.shape[2]
            x = pred
            y = target[:,:,-N:]
            mean_x = torch.mean(x,2,keepdims=True)
            std_x = torch.std(x,2,keepdims=True)
            mean_y = torch.mean(y,2,keepdims=True)
            std_y = torch.std(y,2,keepdims=True)
            x = (x - mean_x)/std_x
            y = (y - mean_y)/std_y
            nor_crosscor = torch.mean(x*y)

        return loss, pred, nor_crosscor
