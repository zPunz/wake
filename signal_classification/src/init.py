
"""
    @date: 12/14/2019
    @author: Tuan Dinh
    @email: dinh5@wisc.edu
    Define all neccessary parameters and variables
"""

import os, sys
import pandas as pd
import numpy as np
from sklearn.model_selection import train_test_split, LeaveOneOut
from sklearn.feature_selection import RFE
from helper import Helper
import argparse

parser = argparse.ArgumentParser()
parser.add_argument('--confidence', type=float, default=0.25, help='prediction confidence threshold')
parser.add_argument('--mode', type=int, default=0, help='testing mode: leave-one-out, k-fold, test-set')
parser.add_argument('--classifier', type=int, default=0, help='id of predefined classifiers')
parser.add_argument('--test_size', type=float, default=0.25, help='fraction of test set')
parser.add_argument('--nfolds', type=int, default=10, help='K in k-fold')
parser.add_argument('--event_threshold', type=float, default=0.2, help='event threshold')
parser.add_argument('--data_path', default='../data/processed', help='path to the data files')
parser.add_argument('--save', default='1211', help='model name')
parser.add_argument('--flag_down', default=True, action='store_false', help='Downsample')
parser.add_argument('--flag_norm', default=True, action='store_false', help='Normalize')
parser.add_argument('--flag_rule', default=False, action='store_true', help='Using heuristic rules for post-processing')
parser.add_argument('--flag_diag', default=False, action='store_true', help='Run the diagonal case')

args = parser.parse_args()

classifier_names = ['rf', 'svm', 'ln', 'xboost', 'stacked']
mode_names = ['loo', 'kfold', 'testset']
LOW_POS_THRESHOLD = 10
filtered_subjects = [26, 27, 29, 30]
filtered_features = []###[0, 1, 2, 3, 4, 5]

result_path = os.path.join('../result/', args.save)
Helper.mkdir(result_path)
