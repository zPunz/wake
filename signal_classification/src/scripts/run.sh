#!/bin/bash
nohup python demo_classify_subjects.py --flag_rule --event_threshold 0.05 > ../result/log05.txt &
nohup python demo_classify_subjects.py --flag_rule --event_threshold 0.1 > ../result/log1.txt &
nohup python demo_classify_subjects.py --flag_rule --event_threshold 0.2> ../result/log2.txt &
nohup python demo_classify_subjects.py --flag_rule --event_threshold 0.3 > ../result/log3.txt &
nohup python demo_classify_subjects.py --flag_rule --event_threshold 0.4 > ../result/log4.txt &
