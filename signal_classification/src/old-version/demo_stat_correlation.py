#!/usr/bin/env python
# coding: utf-8

from _init_ import *

for file in sorted(os.listdir(args.data_path)):
    # load file
    s = file.split('.')
    if not(s[-1] == 'csv'):
        continue
    no = s[0].split('_')[-1]
    csv_path = os.path.join(args.data_path, file)
    df = pd.read_csv(csv_path, header=None)

    y_p1 = df.values[:, -2]
    y_p2 = df.values[:, -3]
    y_t = df.values[:, -1]

    if np.sum(y_t) == 0:
        continue
    # event inds
    inds = (y_p1 < args.event_threshold) * (y_p2 < args.event_threshold)
    y_neg_t = y_t[inds] # number of predicted 0s
    num_zeros = len(y_t) - np.sum(y_t)
    num_zeros_pred = len(y_neg_t) - np.sum(y_neg_t)
    precision = num_zeros_pred /len(y_neg_t)
    recall = num_zeros_pred / num_zeros
    print("{}\t {} \t{} \t{:.4f} \t{:.4f}".format(no, int(num_zeros_pred), int(num_zeros), precision, recall))
