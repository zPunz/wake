#!/usr/bin/env python
# coding: utf-8

import os
import pandas as pd
import numpy as np
from helper import Helper
import matplotlib.pyplot as plt

root = '../data/processed'
today = '1208'
outlier = True
result_path = '../result/features'
Helper.mkdir(result_path)

# positive
def plot_features(featuer_dict, name):
    print(name)
    for k in range(nfeatures):
        print(k)
        fig = plt.figure(figsize=(15, 12))
        # red_square = dict(markerfacecolor='r', marker='s')
        # ax5.boxplot(data, vert=False, flierprops=red_square)
        plt.boxplot([x for x in pos_feature_dict[k]], 0, 'rs', 1, showfliers=outlier)
        plt.xticks([y+1 for y in range(nsubjects)], [str(keys[i]) for i in range(nsubjects)])
        plt.xlabel('Subject')
        plt.ylabel('Feature {}'.format(k))
        plt.title('Box plot')
        save_path = os.path.join(result_path, '{}_feature_{}.png'.format(name, k))
        plt.savefig(save_path)
        plt.close()
        
nfeatures = 76
stats = []
pos_feature_dict = {}
neg_feature_dict = {}
for k in range(nfeatures):
    pos_feature_dict[k] = []
    neg_feature_dict[k] = []

keys = []
nsubjects = 0
for file in sorted(os.listdir(root)):
    # load file
    s = file.split('.')
    if not(s[-1] == 'csv'):
        continue
    nsubjects += 1
    no = s[0].split('_')[-1]
    keys.append(no)
    csv_path = os.path.join(root, file)
    df = pd.read_csv(csv_path, header=None)
    ## donwsample
    df_majority, df_minority = Helper.divide_class(df)
    if len(df_minority) > 10:
        df = Helper.downsample(df_majority, df_minority, len(df_minority))
        # Normalize
        df = Helper.get_normalized(df)
        df_majority, df_minority = Helper.divide_class(df)
        stats.append([int(no), len(df_majority), len(df_minority), len(df_majority) + len(df_minority)])
        for k in range(nfeatures):
            pos_feature_dict[k].append(df_majority.values[:, k])
            neg_feature_dict[k].append(df_minority.values[:, k])

plot_features(pos_feature_dict, 'pos')
plot_features(neg_feature_dict, 'neg')

stats = np.asarray(stats)
print('#-files: ', stats.shape[0])
for i in range(stats.shape[0]):
    print(stats[i, :])
save_path = '../result/num_class.csv'
np.savetxt(save_path, stats, delimiter=',', header='Subject, Neg, Pos, Total')
