# -*- coding: utf-8 -*-
"""
    Created on Sat Mar 23 19:23:37 2019
    Edited on 12/07/2019
    @author: zohre
    @editor: Tuan Dinh
"""

import numpy as np
from matplotlib import pyplot as plt
import matplotlib.mlab as mlab

from os import listdir
from os.path import isfile, join
from scipy import signal
from scipy.stats import kurtosis, skew
import os, psutil, time, glob, itertools, csv, pywt
import pyeeg as px

# Parameters ??
band = [0.5, 2, 4, 6, 8, 10, 12, 14, 16, 30, 40]
theta_range = range(4, 10)
fs = 200 # sample per second
division_pairs = [(0, 1), (4, 6), (6, 9), (2, 9)]
window_size = 5
n_sleep = 3
# segment_width = fs * refreshTime

def mkdir(name, rm=False):
    if not os.path.exists(name):
        os.makedirs(name)

def get_eeg_segment_features(segment):
    signal_band = px.bin_power(segment, band, fs)
    # 1st set
    features = list(signal_band[0][theta_range]) + list(signal_band[1][theta_range])
    # 2nd set
    for x, y in division_pairs:
        features.append(signal_band[1][x] / signal_band[1][y])
    # add some statistics
    features = features + [np.mean(segment), np.std(segment), skew(segment), kurtosis(segment)]
    # 2 channels
    # 2 x 6 signal band 0-1, theta from 4 - 9
    # 4 ratio on band 1: (0, 1), (4, 6), (6, 9), (2, 9)
    # mean, std, skew, kurtosis
    return features

def extract_eeg_features(data, nsegments):
    nchannels = data.shape[1]
    features = []
    for i in range(nsegments):
        row_features = []
        start = i * fs
        end = (i + window_size - 1) * fs + 1
        segments = data[start:end, :]
        for c in range(nchannels):
            segment = segments[:, c]
            c_features = get_eeg_segment_features(segment)
            row_features = row_features + c_features
        features.append(row_features)

    return features

def extract_efeatures(data, nsegments):
    features = []
    for i in range(nsegments):
        start = i * fs
        end = (i + window_size - 1) * fs + 1
        segment = data[start:end]
        seg_features = [np.mean(segment), np.std(segment), skew(segment), kurtosis(segment), np.min(segment), np.max(segment)]
        features.append(seg_features)

    return features

##### GET #######
def get_eeg_features(data, start, end):
    row_features = []
    nchannels = data.shape[1]
    segments = data[start:end, :]
    for c in range(nchannels):
        segment = segments[:, c]
        c_features = get_eeg_segment_features(segment)
        row_features = row_features + c_features
    return row_features

def get_efeatures(data, start, end):
    segment = data[start:end]
    seg_features = [np.mean(segment), np.std(segment), skew(segment), kurtosis(segment), np.min(segment), np.max(segment)]
    return seg_features


def get_emg_event_features(data, start, end):
    segment = data[start:end]
    # count number of event
    # count = np.sum(segment > 0); [count / np.size(segment)]
    return [1 / np.exp(np.mean(segment))]


def get_blink_features(data, start, end):
    segment = data[start:end]
    # % First order derivative of the blink signal.
    diff_sig = np.diff(segment)
    diff_sig = [0] + list(diff_sig)

    # % Closing velocity th and opening vel threshold
    clo_v_th = -1 * (np.abs(np.min(diff_sig)) - 0.15)
    open_v_th = -1 * clo_v_th

    # % 1. Blink amplitude
    blink_amplitude = np.abs(np.min(segment)) - np.mean(segment)

    # % 2. Mean amplitude
    mean_amp = np.mean(segment)

    # % 3. Peak closing velocity
    peak_min_idx = np.argmin(diff_sig)
    peak_closing_vel = diff_sig[peak_min_idx]

    # % 4. Peak opening velocity
    peak_max_idx = np.argmax(diff_sig)
    peak_opening_vel = diff_sig[peak_max_idx]

    # % 7. closing time
    close_times = np.argwhere(diff_sig < clo_v_th).flatten() # index

    cl_points_idx_1 = np.argwhere(close_times > peak_min_idx - 100).flatten()
    cl_points_idx_2 = np.argwhere(close_times < peak_min_idx + 100).flatten()
    cl_points_idx = list(set(cl_points_idx_1) & set(cl_points_idx_2))
    close_times = close_times[cl_points_idx]
    close_times = (np.max(close_times) - np.min(close_times)) / fs

    # % 5. Mean closing velocity
    mean_cl_v = np.mean(np.asarray(diff_sig)[cl_points_idx])

    featuresBlink = [blink_amplitude, mean_amp, peak_closing_vel, peak_opening_vel, mean_cl_v, close_times]

    return featuresBlink


def get_saccade_features(data, start, end):
    segment = data[start:end]
    # % First order derivative of the blink signal.
    velocity = np.diff(segment)*fs*fs
    acceleration = np.diff(velocity)*fs*fs

    # % 1. Mean velocity
    meanVel = np.mean(velocity)

    # % 2. Maximum velocity
    maxVel = np.max(velocity)

    # % 3. Mean acceleration
    meanAcc = np.mean(acceleration)

    # % 4. Maximum acceleration
    maxAcc = np.max(acceleration)

    # % 5. Range amplitude
    rangeAmp = np.max(segment) - np.min(segment)

    featureSaccade = [meanVel, maxVel, meanAcc, maxAcc, rangeAmp]

    return featureSaccade


def label_second(label_path, nseconds):
    # number of segments:
    labels = np.zeros(nseconds)
    # get segments of positive labels
    print(label_path)
    pos_segs = []
    with open(label_path) as f:
        # read file eeg
        f_csv = csv.reader(f)
        headers = next(f_csv)
        for row in f_csv:
            pos_segs.append(row)
    for row in range(len(pos_segs)):
        # time
        [m, s, ms] = pos_segs[row][0].split('.')
        start = int(m) * 60 + int(s) + (int(ms) + 5) // 10
        [m, s, ms] = pos_segs[row][1].split('.')
        end = int(m) * 60 + int(s) + (int(ms) + 5) // 10
        if end == nseconds:
            end = end - 1
        for i in range(start, end + 1):
            labels[i] = 1
    return labels

def skipped_second(skip_path, nseconds):
    # number of segments:
    skips = np.zeros(nseconds)
    # get segments of positive skips
    print(skip_path)
    pos_segs = []
    with open(skip_path) as f:
        # read file eeg
        f_csv = csv.reader(f)
        headers = next(f_csv)
        for row in f_csv:
            pos_segs.append(row)

    for row in range(len(pos_segs)):
        # time
        [m, s, ms] = pos_segs[row][0].split('.')
        start = int(m) * 60 + int(s) + (int(ms) + 5) // 10
        [m, s, ms] = pos_segs[row][1].split('.')
        end = int(m) * 60 + int(s) + (int(ms) + 5) // 10
        if end == nseconds:
            end = end - 1
        for i in range(start, end+1):
            skips[i] = 1

    return skips

def main():
    # root
    wake_path = '../../'
    result_path = os.path.join(wake_path, 'data/processed/')
    mkdir(result_path)

    # Subjects
    subjects = [9, 10, 12, 13, 15, 16, 19, 21, 22, 23, 24, 25, 26, 27, 28, 29, 30, 31, 33, 34, 35, 37, 38, 39, 40, 41]
    subjects_w2sess = [21, 25, 27, 35, 38, 40, 41]
    # Bad sections including the 'good one'
    bad_sess_dict = {1: [11, 14, 16, 17, 18, 19, 20, 21, 26, 27, 29, 30, 36, 38, 40],
                    2: [10, 11, 12, 14, 17, 18, 20, 21, 22, 25, 26, 27, 28, 29, 30, 31, 34, 36]}
    # skip labels
    bad_sess_dict_full = {1: [11, 14, 17, 18, 20, 36, 29, 38, 40],
                    2: [11, 14, 17, 18, 20, 36, 21, 27, 28]}

    feature_names = []
    """
    # eeg
    2 channels, theta_range: 4-9
    # 2 channels
    # 2 x 6 signal band 0-1, theta from 4 - 9
    # 4 ratio on band 1: (0, 1), (4, 6), (6, 9), (2, 9)
    # mean, std, skew, kurtosis

    # ['EOGh', 'EOGv', 'EDA', 'EMG']
    # mean, std, skew, kurtosis, min, max

    # blink: [blink_amplitude, mean_amp, peak_closing_vel, peak_opening_vel, mean_cl_v, close_times]

    # saccade: ['meanVel', 'maxVel', 'meanAcc', 'maxAcc', 'rangeAmp']
    # event emg: 2
    # label
    """

    for subj in [41]:
        # if subj >= 28:
        #     break
        print(subj)
        # number of sessions
        if subj in subjects_w2sess:
            nsessions = 1
        else:
            nsessions = 2
        # whole features
        features = []
        for session in range(1, 1 + nsessions):
            # skip bad session
            if subj in bad_sess_dict_full[session]:
                continue
            # path to e-signals
            e_path = os.path.join(wake_path, "{:02d}/Session{}/ProcessedData/".format(
            subj, session))
            dict_data = {}
            for s in ['EEG', 'EOGh', 'EOGv', 'EDA', 'EMG', 'EMG_active_detection', 'EMG_active_detection_2']:
                s_path = os.path.join(e_path, "filtered_WAKE_{}.csv".format(s))
                if not(os.path.exists(s_path)):
                    s_path = os.path.join(e_path, "Filtered_WAKE_{}.csv".format(s))
                signals = np.loadtxt(s_path, delimiter=',')
                if s in ['EDA', 'EMG']:
                    signals = signals[:, 1]
                dict_data[s] = signals

            # features
            nseconds = dict_data['EEG'].shape[0] // fs # number of seconds
            nsegments = nseconds - window_size + 1 # number of segments, with stride 1s
            # labels
            label_path = os.path.join(e_path, "uSleepEpisodes.csv")
            if not(os.path.exists(label_path)):
                label_path = os.path.join(e_path, "uSleepEpiosdes.csv")
            skip_path = os.path.join(e_path, "skippedEpisodes.csv")
            if not(os.path.exists(skip_path)):
                skip_path = os.path.join(e_path, "skippedEpiosdes.csv")

            label_seconds = label_second(label_path, nseconds)
            skip_seconds = skipped_second(skip_path, nseconds)
            sess_features = []
            num_features = 0
            for i in range(nsegments):
                start_second = i
                end_second = i + window_size - 1
                start = start_second * fs
                end = end_second * fs + 1
                # skip
                if np.sum(skip_seconds[start_second:end_second + 1]) >= 1:
                    continue
                # for eeg
                row_features = get_eeg_features(dict_data['EEG'], start, end)
                print(len(row_features), 'EEG')
                # for EDA
                for s in ['EOGh', 'EOGv', 'EDA', 'EMG']:
                    features = get_efeatures(dict_data[s], start, end)
                    row_features = row_features + features
                    print(len(features), s)
                    num_features += len(features)
                # blink
                features = get_blink_features(dict_data['EOGv'], start, end)
                row_features = row_features + features
                print(len(features), 'blink')
                # saccade
                features = get_saccade_features(dict_data['EOGh'], start, end)
                row_features = row_features + features
                print(len(features), 'saccade')
                # EMG event
                features = get_emg_event_features(dict_data['EMG_active_detection'], start, end)
                row_features = row_features + features
                print(len(features), 'emg events 1')
                features = get_emg_event_features(dict_data['EMG_active_detection_2'], start, end)
                row_features = row_features + features
                print(len(features), 'emg events 2')
                # label
                if np.sum(label_seconds[start_second:end_second + 1]) >= n_sleep:
                    row_features.append(1)
                else:
                    row_features.append(0)

                print('labels')
                print('total: ', len(row_features))
                from IPython import embed; embed()
                sess_features.append(row_features)

            sess_features = np.asarray(sess_features)
            print('session')
            if len(features) == 0:
                features = sess_features
            else:
                features = np.concatenate([features, sess_features], axis=0)

        if len(features) == 0:
            print('Nothing here')
        else:
            savefile = os.path.join(result_path, "feature_{}.csv".format(subj))
            print("Save: ", savefile)
            np.savetxt(savefile, features, delimiter=",", fmt='%s')

if __name__ == '__main__':
    main()
