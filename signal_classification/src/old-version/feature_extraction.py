# -*- coding: utf-8 -*-
"""
    Created on Sat Mar 23 19:23:37 2019
    Edited on 12/07/2019
    @author: zohre
    @editor: Tuan Dinh
"""

import numpy as np
from matplotlib import pyplot as plt
import matplotlib.mlab as mlab

from os import listdir
from os.path import isfile, join
from scipy import signal
from scipy.stats import kurtosis, skew, sp
import os, psutil, time, glob, itertools, csv, pywt

import pyeeg as px

"""
    Skip bad segments
    Divide the signals into
"""
def labeling(label_path, eeg, fs, refreshTime, windowSize):
    # number of segments:
    nsegments = len(eeg) // fs
    labels = np.zeros(nsegments)
    # get segments of positive labels
    print(label_path)
    pos_segs = []
    with open(label_path) as f:
        # read file eeg
        f_csv = csv.reader(f)
        headers = next(f_csv)
        for row in f_csv:
            pos_segs.append(row)
    #
    for row in range(len(pos_segs)):
        # time
        [m, s, ms] = pos_segs[row][0].split('.')
        start = int(m) * 60 + int(s) + int(ms) * 0.1
        [m, s, ms] = pos_segs[row][1].split('.')
        end = int(m) * 60 + int(s) + int(ms) * 0.1
        for i in range(start, end):
            labels[i] = 1

    return labels

"""
    Feature extraction on each signal
    Slice Window
"""
def FeatureExtractionTemporal(data, fs, windowSize, refreshTime, nfeatures):
    segment_width = refreshTime * fs
    nsegments = len(data) // segment_width
    features = np.zeros([nsegments, nfeatures])
    for i in range(nsegments):
        j = i * segment_width
        segment = data[j:j + segment_width]
        features[i] = np.asarray([np.mean(segment), np.std(segment), skew(segment), kurtosis(segment), np.min(segment), np.max(segment)])

    return features

"""
    Feature Extraction for EEG
"""
band = [0.5, 2, 4, 6, 8, 10, 12, 14, 16, 30, 40]
alpha = 9
fs = 200
division_pairs = [(0, 1), (4, 6), (6, 9), (2, 9)]
segment_width = fs * refreshTime

def get_eeg_segment_features(segment):
    signal_band = px.bin_power(segment, band, fs)
    # 1st set
    features = list(signal_band[:, :alpha].flatten())
    # 2nd set
    for x, y in division_pairs:
        features.append(signal_band[1][x] / signal_band[1][y])
    # add some statistics
    features = features + [np.mean(segment), np.std(segment), skew(segment), kurtosis(segment)]
    return features

def extract_eeg_features(eeg):
    features = []
    for i in range(nsegments):
        row_features = []
        j = i * segment_width
        eeg_segment = eeg[j: j + segment_width, :]
        for c in range(len(nchannels)):
            segment = eeg_segment[:, nchannels[c]]
            seg_features = get_eeg_segment_features(segment)
            row_features = row_features + seg_features
        features.append(row_features)

    return features

def extract_efeatures(data):
    features = []
    for i in range(nsegments):
        j = i * segment_width
        segment = data[j:j + segment_width]
        seg_features = [np.mean(segment), np.std(segment), skew(segment), kurtosis(segment), np.min(segment), np.max(segment)]
        features.append(row_features)

    return features

def featureExtraction_EEG(data, labels, fs, nchannels, windowSize, refreshTime, nfeatures):

    # setting
    segment_width = refreshTime * fs
    nsegments = len(data) // segment_width
    cols_count = nfeatures * len(nchannels)
    features = np.zeros([nsegments, cols_count])

    #  for slinding window j=i+windowSize
    # each row is a segment

    for i in range(nsegments):
        j = i * segment_width
        count = alpha * 2
        for c in range(len(nchannels)):
            segment = data[j: j + segment_width, nchannels[c]]
            # power=Power(segment)
            signal_band = px.bin_power(segment, band, fs)
            # delta1 Abs && Theta1 Ratio
            start = c * nfeatures
            # update features
            features[i][start: start + count - 1] = signal_band[:, :alpha].flatten()
            for x, y in division_pairs:
                features[i][start + count] = signal_band[1][x] / signal_band[1][y]
                count += 1
            features[i][start + count :] = np.asarray([np.mean(segment), np.std(segment), skew(segment), kurtosis(segment)])

    return features


def main():
    # root
    wake_path = '../../'

    # Parameters ??
    nchannels = [1]
    fs = 200 # sample per second
    windowSize = 0
    refreshTime = 5
    nfeatures = 27
    nfeaturesTemporal = 6
    segment_width = refreshTime * fs

    # Subjects
    subjects = [9, 10, 12, 13, 15, 16, 19, 21, 22, 23, 24, 25, 26, 27, 28, 29, 30, 31, 33, 34, 35, 37, 38, 39, 40]
    subjects_w2sess = [21, 25, 27, 35, 38, 40]
    # Bad sections including the 'good one'
    bad_sess_dict = {1: [11, 14, 16, 17, 18, 19, 20, 21, 26, 27, 29, 30, 36, 40], 2: [10, 11, 12, 14, 17, 18, 20, 21, 22, 25, 26, 27, 28, 29, 30, 31, 34, 36]}
    # skip labels

    for subj in subjects:
        # number of sessions
        if subj in subjects_w2sess:
            nsessions = 1
        else:
            nsessions = 2
        # whole features
        features = []
        for session in range(1, 1 + nsessions):
            # skip bad session
            if subj in bad_sess_dict[session]:
                continue

            # path to e-signals
            e_path = os.path.join(wake_path, "{:02d}/Session{}/ProcessedData/".format(
            subj, session))
            if not(os.path.exists(e_path)):
                continue

            # EEG
            eeg_path = os.path.join(e_path, "filtered_WAKE_EEG.csv")
            label_path = os.path.join(e_path, "uSleepEpisodes.csv")
            eeg = np.loadtxt(eeg_path, delimiter=',')
            labels = labeling(
                label_path, eeg, fs, refreshTime, windowSize)
            eeg_feature = featureExtraction_EEG(
                eeg, labels, fs, nchannels, windowSize, refreshTime, nfeatures)

            # the rest e-signals
            e_feature = eeg_feature
            for s in ['EOGh', 'EOGv', 'EDA', 'EMG']:
                if s == 'EDA':
                    s_path = os.path.join(e_path, "Filtered_WAKE_{}.csv".format(s))
                else:
                    s_path = os.path.join(e_path, "filtered_WAKE_{}.csv".format(s))
                signals = np.loadtxt(s_path, delimiter=',')
                if s in ['EDA', 'EMG']:
                    signals = signals[:, 1]
                s_feature = FeatureExtractionTemporal(
                    signals, fs, windowSize, refreshTime, nfeaturesTemporal)
                e_feature = np.concatenate([s_feature, e_feature], axis=1)

            # other features
            c_path = os.path.join(wake_path, "Nam/EOG_features")
            blink_path = "{}/Blink_features/featureBlink{:02d}s{}_{}.csv".format(c_path, subj, session, refreshTime)
            saccade_path = "{}/Saccade_features/featureSaccade{:02d}s{}_{}.csv".format(c_path, subj, session, refreshTime)
            eog_features_blink = np.loadtxt(blink_path, delimiter=',')
            eog_features_saccade = np.loadtxt(saccade_path, delimiter=',')
            c_feature = np.column_stack((eog_features_blink,  eog_features_saccade))

            # combine: label at the end
            ec_feature = np.concatenate([c_feature, e_feature], axis=1)
            if len(features) == 0:
                features = ec_feature
            else:
                features = np.concatenate([features, ec_feature], axis=0)

        # savefile = os.path.join(wake_path, "data/processed/feature_{}.csv".format(subj))
        savefile = os.path.join(wake_path, "data/processed/feature_names.csv")

        print("Save: ", savefile)
        np.savetxt(savefile, features, delimiter=",", fmt='%s')
        print('Done name features')
        break

if __name__ == '__main__':
    main()
