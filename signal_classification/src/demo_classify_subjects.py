"""
    @date: 12/14/2019
    @author: Tuan Dinh
    @email: dinh5@wisc.edu
    Classify microsleep segments using all subjects
"""

from sklearn.tree import DecisionTreeClassifier
from _init_ import *
import warnings
warnings.filterwarnings("ignore")

"""
    Concatenate all data frames except the k-th one
    @params:
        dfs: list of data frames
        keys [ints]: list of integeral keys
        k [int]: the filtered key
    @return unified dataframe
"""
def unify(dfs, keys, k):
    count = 0
    for l in keys:
        if not (l == k):
            if count == 0:
                df = dfs[l]
            else:
                df = df.append(dfs[l])
    return df

"""
    Load data from csv files
    @params:
        root: path to data directory
        flag_downsample: downsample the negative class if the flag is True
        flag_normalize: upsample the positive class if the flag is True
    @return a dictionary of all file data frames
"""
def get_dfs(root, flag_downsample=False, flag_normalize=False):
    dfs = {} # dictionary of data frames
    keys = [] #
    for file in sorted(os.listdir(root)):
        s = file.split('.')
        if not(s[1] == 'csv'):
            continue
        no = int(s[0].split('_')[-1])
        # load data
        csv_path = os.path.join(root, file)
        df = pd.read_csv(csv_path, header=None)
        # filter features
        df = Helper.reject_outliers(df)
        df_majority, df_minority = Helper.divide_class(df)
        # filter minor subjects
        if len(df_minority) == 0:
            print("Filtered: ", no)
            continue
        # downsample
        if flag_downsample and (len(df_minority) < len(df_majority)):
            df = Helper.downsample(df_majority, df_minority, sample_size=int(len(df_minority)))
        ## normalize data
        if flag_normalize:
            df = Helper.get_normalized(df)
        # assign
        dfs[no] = df
        from IPython import embed; embed()
        keys.append(no)
    return dfs, keys


"""
    Trainning
"""
def run():
    '''
        load data
    '''
    dfs, keys = get_dfs(args.data_path, args.flag_down, args.flag_norm)
    print(keys)

    '''
        Feature selection
    '''
    # feature selection: [N-1]
    # selected_features = select_feature(dfs, keys)
    # print('#-features: ', np.sum(selected_features))
    # # lablel
    # selected_features = np.append(selected_features, True)
    # # last 2
    # selected_features[-2] = True
    # selected_features[-3] = True
    # for k in keys:
    #     d = dfs[k].values[:, selected_features]
    #     dfs[k] = pd.DataFrame(data=d)

    arr = []
    for k in keys:
        df_test = dfs[k]
        df_train = unify(dfs, keys, k)

        # prepare training and testing data
        X_train, y_train, class_weight = Helper.prepare_data(df_train)
        X_test, y_test, _ = Helper.prepare_data(df_test)

        # initialize 3 classifiers for the hierachical system
        classifier1 = Helper.get_classifier(0, class_weight)
        classifier1.fit(X_train, y_train)
        classifier2 = Helper.get_classifier(4, class_weight)
        classifier2.fit(X_train, y_train)
        classifier3 = Helper.get_classifier(1, class_weight)
        classifier3.fit(X_train, y_train)

        # 1st layer
        y_pred = classifier1.predict_proba(X_test)
        score = np.max(y_pred, axis=1)
        y_p = np.argmax(y_pred, axis=1)
        # if the confidence is low
        inds = score < 0.7
        X2_test = X_test[inds, :]
        if X2_test.shape[0] > 0:
            # 2nd layer
            y2_pred = classifier2.predict_proba(X2_test)
            score2 = np.max(y2_pred, axis=1)
            y2_p = np.argmax(y2_pred, axis=1)
            # if the confidence is low
            inds2 = score2 < 0.7
            X3_test = X2_test[inds2, :]
            if X3_test.shape[0] > 0:
                # last layer
                y3_p = classifier3.predict(X3_test)
                y2_p[inds2] = y3_p
            y_p[inds] = y2_p

        # heuristics for post-proccessing
        y_pred = y_p
        if args.flag_rule:
            inds = (X_test[:, -1] < args.event_threshold)*(X_test[:, -2] < args.event_threshold)
            y_pred[inds] = 0

        # y_pred = Helper.classify(classifier, X_test, args.flag_rule, args.event_threshold)
        row = Helper.get_score(y_test, y_pred)
        arr.append([k] + row)

    # result out
    for i in range(len(arr)):
        print(arr[i])
    print(np.mean(np.asarray(arr), axis=0))
    save_path = os.path.join(result_path, 'subj_{}_{}.csv'.format(mode_names[args.mode], classifier_names[args.classifier]))
    np.savetxt(save_path, np.asarray(arr), delimiter=',', fmt='%.2e', header='Subject, Precision, Recall')
    print('Done')
'''
# Testing procedures
def run_diag():
    arr = []
    dfs, keys = get_dfs(args.data_path, args.flag_down, args.flag_norm)
    for k in keys:
        df_test = dfs[k]
        df_train = df_test
        # prepare
        X_train, y_train, class_weight = Helper.prepare_data(df_train, flag_downsample=not(args.flag_down), flag_normalize=not(args.flag_norm))
        X_test, y_test, _ = Helper.prepare_data(df_test, flag_downsample=not(args.flag_down), flag_normalize=not(args.flag_norm))
        #classifier
        classifier = Helper.get_classifier(args.classifier, class_weight)
        y_pred = Helper.classify(classifier, X_train, y_train, X_test, args.flag_rule, args.event_threshold)
        row = Helper.get_score(y_test, y_pred)
        print(k, row)


def run_matrix(ta=0.7, tb=0.7, off_1=0, off_2=4, off_3=1):
    dfs, keys = get_dfs(args.data_path, args.flag_down, args.flag_norm)
    print(keys)

    selected_features = select_feature(dfs, keys)
    print('#-features: ', np.sum(selected_features))
    # lablel
    selected_features = np.append(selected_features, True)
    # last 2
    selected_features[-2] = True
    selected_features[-3] = True
    for k in keys:
        d = dfs[k].values[:, selected_features]
        dfs[k] = pd.DataFrame(data=d)

    lst = [13, 15, 19, 24, 25, 31, 33, 35]
    classifier1_dict = {}
    classifier2_dict = {}
    classifier3_dict = {}
    means = {}
    for k in keys:
        df_train = dfs[k]
        # prepare
        X_train, y_train, class_weight = Helper.prepare_data(df_train)
        means[k] = np.mean(X_train, axis=0)
        #classifier
        classifier = Helper.get_classifier(off_1, class_weight)
        classifier.fit(X_train, y_train)
        classifier1_dict[k] = classifier

        classifier = Helper.get_classifier(off_2, class_weight)
        classifier.fit(X_train, y_train)
        classifier2_dict[k] = classifier

        classifier = Helper.get_classifier(off_3, class_weight)
        classifier.fit(X_train, y_train)
        classifier3_dict[k] = classifier

    # distances
    distances = {}
    for k in keys:
        dict_k = []
        s = 0
        for j in keys:
            if not(j == k):
                d = np.exp(-np.linalg.norm(means[k] - means[j])**2/2)
                s = s + d
                dict_k.append(d/s)

        distances[k] = np.asarray(dict_k)

    def foo(m, X_test, y_test, classify_mode=0, t2=1.5):
        y = []
        # outliers = [15, 19, 23, 24, 31, 33, 35]
        for k in keys:
            if not(k == m):
                if classify_mode == 0:
                    y_pred = classifier1_dict[k].predict_proba(X_test)
                    score = np.max(y_pred, axis=1)
                    y_p = np.argmax(y_pred, axis=1)
                    # low confidence
                    inds = score < ta
                    X2_test = X_test[inds, :]
                    if X2_test.shape[0] > 0:
                        y2_pred = classifier2_dict[k].predict_proba(X2_test)
                        score2 = np.max(y2_pred, axis=1)
                        y2_p = np.argmax(y2_pred, axis=1)
                        # low confidence
                        inds2 = score2 < tb
                        X3_test = X2_test[inds2, :]
                        if X3_test.shape[0] > 0:
                            y3_p = classifier3_dict[k].predict(X3_test)
                            y2_p[inds2] = y3_p
                        y_p[inds] = y2_p
                elif classify_mode == 1:
                    y1_p = classifier1_dict[k].predict(X_test)
                    y2_p = classifier2_dict[k].predict(X_test)
                    y3_p = classifier3_dict[k].predict(X_test)
                    y_p = (y1_p + y2_p + y3_p > t2).astype(int)
                else:
                    y_p = classifier2_dict[k].predict(X_test)
                y.append(y_p)
                # print scores
                # y_k = y_p
                # inds = (X_test[:, -1] < args.event_threshold)*(X_test[:, -2] < args.event_threshold)
                # y_k[inds] = 0
                # row = Helper.get_score(y_test, y_k)
                # print(k, row)
        return y

    def exe(lst, classify_mode, t2=1.5):
        arr = []
        for m in [31, 33, 34, 35, 15, 16]:
            df_test = dfs[m]
            X_test, y_test, _ = Helper.prepare_data(df_test, flag_downsample=not(args.flag_down), flag_normalize=not(args.flag_norm))
            y = foo(m, X_test, y_test, classify_mode, t2)
            # weighted mean
            y = np.average(np.asarray(y), weights=distances[m], axis=0)

            y_pred = (y > args.confidence).astype(int)
            # rules
            inds = (X_test[:, -1] < args.event_threshold)*(X_test[:, -2] < args.event_threshold)
            y_pred[inds] = 0
            row = Helper.get_score(y_test, y_pred)
            arr.append(row)
            print(m, row)
        # print out
        result = np.mean(np.asarray(arr), axis=0)
        print(result)
        if result[0] > 0.8 and result[1] > 0.8:
            print('BINGO!!!')

    print('Mode 0')
    exe(lst, classify_mode=0)
    print('Mode 1')
    exe(lst, classify_mode=1, t2=1)
    print('Mode 2')
    exe(lst, classify_mode=2)
'''

"""
    Feature selection
    @params:
        dfs: dictionary of data frames
        keys: list of keys
    @returns list of features
"""
def select_feature(dfs, keys):
    from sklearn.ensemble import ExtraTreesClassifier
    forest = ExtraTreesClassifier(n_estimators=250,
                              random_state=0)

    df = unify(dfs, keys, k=-1)
    scores = []
    X, y, class_weight = Helper.prepare_data(df)
    # clf = DecisionTreeClassifier()
    # clf.fit(X, y)
    forest.fit(X, y)
    importances = forest.feature_importances_

    print("Feature ranking:")
    scores = np.asarray(importances)
    indices = np.argsort(-scores)
    topk = indices[:10]
    print(topk)
    for f in topk:
        print("feature {}: {}".format(f, scores[f]))


    from IPython import embed; embed()

if __name__ == '__main__':
    print('Rule: ', args.flag_rule)
    print('Pos confidence: ', args.confidence)
    print('Event threshold: ', args.event_threshold)
    # run_matrix()
    dfs, keys = get_dfs(args.data_path, args.flag_down, args.flag_norm)
    print(keys)
    select_feature(dfs, keys)

    # run()
    # select_feature()
    # for ta in [0.5, 1.5, 2.5]:
    #     # for tb in [0.65, 0.75, 0.8, 0.9]:
    #     for off_1, off_2, off_3 in [(0, 3, 2), (3, 0, 2), (0, 4, 1), (4, 0, 1)]:
    #         print("Set: ", ta, tb, off_1, off_2, off_3)
    #         run_matrix(ta, tb, off_1, off_2, off_3)
