"""
	@author Tuan Dinh tuandinh@cs.wisc.edu
	@date 08/14/2019
	Loading data
"""



import torch
import torch.nn as nn
import torch.nn.functional as F
import torch.autograd as autograd
import torch.optim as optim
from torch.optim import lr_scheduler
from torch.utils.data import DataLoader
from torch.utils.tensorboard import SummaryWriter

import os, sys, time
import numpy as np
from sklearn.metrics import classification_report, confusion_matrix, precision_score, recall_score
import argparse

from utils.provider import Provider

parser = argparse.ArgumentParser()
parser.add_argument('--num_epochs', type=int, default=100)
parser.add_argument('--batch_size', type=int, default=64)
parser.add_argument('--lr', type=float, default=0.1)
parser.add_argument('--log_step', type=int, default=10, help='step size for prining log info')
parser.add_argument('--flag_retrain', default=False, action='store_true', help='Re train')
# Model parameters
args = parser.parse_args()

################## Setting #######################
model_name='Wake'
logf=open("log.txt", 'w')
writer=SummaryWriter(comment = 'wake', log_dir = 'runs')

#####====================== Data ================######
device = torch.device('cuda' if torch.cuda.is_available() else 'cpu')

x_train, x_test, y_train, y_test = load_data(up=True)
x_train = torch.Tensor(x_train).to(device)
y_train = torch.Tensor(y_train).to(device)
x_test = torch.Tensor(x_test).to(device)
y_test = torch.Tensor(y_test).to(device)

trainset = torch.cat([x_train, y_train.unsqueeze(1)], dim=1)
testset = torch.cat([x_test, y_test.unsqueeze(1)], dim=1)

dataloader = DataLoader(trainset, batch_size = args.batch_size,
                        shuffle = True, drop_last = True)

testloader = DataLoader(testset, batch_size = args.batch_size,
                        shuffle = True, drop_last = True)

####====== Modules =======####
def log_loss(epoch, step, total_step, loss, start_time):
    # convert
    loss=loss.cpu().data.numpy()
    # msg
    message='Epoch [{}/{}], Step [{}/{}], Loss: {:.4f}, time: {:.4f}s'.format(
        epoch, args.num_epochs, step, total_step, loss, time.time() - start_time)
    # log out
    Helper.log(logf, message)

def weights_init(m):
		classname = m.__class__.__name__
		if classname.find('Linear') != -1:
			m.weight.data.normal_(0.0, 0.02)
			m.bias.data.fill_(0)
		elif classname.find('Conv') != -1:
			torch.nn.init.normal_(m.weight.data, 0.0, 0.02)
		elif classname.find('BatchNorm') != -1:
			m.weight.data.normal_(1.0, 0.02)
			m.bias.data.fill_(0)

def get_scores(y_test, y_pred, onecode=True):
    y_t = y_test.cpu().data.numpy()
    y_p = y_pred.cpu().data.numpy()
    if onecode:
        y_p = np.argmax(y_p, axis=1)
    else:
        y_p = [0 if e < 0.5 else 1 for e in y_p]
    # print(confusion_matrix(y_test, y_pred))
    # print(classification_report(y_test, y_pred, labels=1))
    return precision_score(y_t, y_p, labels=1), recall_score(y_t, y_p, labels=1)
