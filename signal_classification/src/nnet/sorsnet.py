import torch.nn as nn

class SorsNet(nn.Module):
    def __init__(self, in_channels, num_linear_units, num_classes):
        super(SorsNet, self).__init__()

        def block(in_c, out_c, ksize=7, stride=2, pad=3):
            return [nn.Conv1d(in_channels=in_c, out_channels=out_c, kernel_size=ksize, stride=stride, padding=pad),           nn.BatchNorm1d(out_c), nn.ReLU(inplace=True)]

        block_list = block(in_channels, 128)
        for _ in range(5):
            block_list += block(128, 128)

        block_list += block(128, 256)
        for _ in range(3):
            block_list += block(256, 256, ksize=5, pad=2)
        for _ in range(2):
            block_list += block(256, 256, ksize=3, pad=1)
        self.conv = nn.Sequential(*block_list)

        self.fc = nn.Sequential(
            nn.Linear(num_linear_units, 100),
            nn.BatchNorm1d(100),
            nn.ReLU(inplace=True),
            nn.Linear(100, num_classes),
            nn.LogSoftmax(dim=1)
        )

    def embed(self, x):
        out = self.conv(x)
        out = out.view(out.shape[0], -1)
        return out

    def forward(self, x):
        return self.fc(self.embed(x))
