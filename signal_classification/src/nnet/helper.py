"""
	@date: 04/02/2020
	@author: Tuan Dinh
	@email: tuan.dinh@wisc.edu
	Classify sleep stages
"""

import os
import pandas as pd
import numpy as np
from sklearn.metrics import precision_score, recall_score, accuracy_score

class AverageMeter(object):
	"""Computes and stores the average and current value"""
	def __init__(self):
		self.reset()

	def reset(self):
		self.val = 0
		self.avg = 0
		self.sum = 0
		self.count = 0

	def update(self, val, n=1):
		self.val = val
		self.sum += val * n
		self.count += n
		self.avg = self.sum / self.count



class Helper:

	######===================== ops ==================############
	@staticmethod
	def mkdir(name, rm=False):
		if not os.path.exists(name):
			os.makedirs(name)

	@staticmethod
	def get_hms(seconds):
		m, s = divmod(seconds, 60)
		h, m = divmod(m, 60)
		return h, m, s


	######===================== training network ==================############
	@staticmethod
	def weights_init(m):
		classname = m.__class__.__name__
		if classname.find('Conv') != -1:
			m.weight.data.normal_(0.0, 0.02)
		elif classname.find('BatchNorm') != -1:
			m.weight.data.normal_(1.0, 0.02)
			m.bias.data.fill_(0)

	#iresnet
	@staticmethod
	def learning_rate(init, epoch):
		optim_factor = 0
		if epoch > 160:
			optim_factor = 3
		elif epoch > 120:
			optim_factor = 2
		elif epoch > 60:
			optim_factor = 1
		return init*math.pow(0.2, optim_factor)

	@staticmethod
	def update_lr(optimizer, lr):
		for param_group in optimizer.param_groups:
			param_group['lr'] = lr

	######===================== metrics ==================############
	@staticmethod
	def accuracy(output, target, topk=(1,)):
		"""Computes the precision@k for the specified values of k"""
		maxk = max(topk)
		batch_size = target.size(0)

		_, pred = output.topk(maxk, 1, True, True)
		pred = pred.t()
		correct = pred.eq(target.view(1, -1).expand_as(pred))

		res = []
		for k in topk:
			correct_k = correct[:k].view(-1).float().sum(0)
			res.append(correct_k.mul_(100.0 / batch_size))
		return res

	@staticmethod
	def get_score(y_t, y_p):
		return [precision_score(y_t, y_p, labels=1), recall_score(y_t, y_p, labels=1)]


	######===================== data processing ==================############
	@staticmethod
	def get_normalized(df, off=3):
		# feature : not the last 3: 2 events + labels
		d = df.values
		X = d[:, :-off]
		x_mean = np.mean(X, axis=0)
		x_std = np.std(X, axis=0)
		x_min = np.min(X, axis=0)
		x_max = np.max(X, axis=0)
		X = (X - x_mean)/x_std
		d[:, :-off] = X
		return pd.DataFrame(data=d)

	@staticmethod
	def reject_outliers(df, off=3):
		def filter(data, m=10):
			return abs(data - np.median(data)) < m * np.std(data)
		# feature : not the last 3: 2 events + labels
		X = df.values
		for i in range(X.shape[1] - off):
			inds = filter(X[:, i])
			X = X[inds, :]
		return pd.DataFrame(data=X)
