

import torch.nn as nn


def conv3x3(in_planes, out_planes, stride=1):
    """3x3 convolution with padding"""
    return nn.Conv1d(in_planes, out_planes, kernel_size=3, stride=stride,
                     padding=1, bias=False)

class BasicBlock3x3(nn.Module):
    expansion = 1

    def __init__(self, inplanes3, planes, stride=1, downsample=None):
        super(BasicBlock3x3, self).__init__()
        self.conv1 = conv3x3(inplanes3, planes, stride)
        self.bn1 = nn.BatchNorm1d(planes)
        self.relu = nn.ReLU(inplace=True)
        self.conv2 = conv3x3(planes, planes)
        self.bn2 = nn.BatchNorm1d(planes)
        self.downsample = downsample
        self.stride = stride

    def forward(self, x):
        residual = x

        out = self.conv1(x)
        out = self.bn1(out)
        out = self.relu(out)
        out = self.conv2(out)
        out = self.bn2(out)

        if self.downsample is not None:
            residual = self.downsample(x)

        out += residual
        out = self.relu(out)

        return out



class Net(nn.Module):
    def __init__(self, dim_in=61, dim_out=1):
        super(Net, self).__init__()

        hiddens = [128, 64, 32]
        def block(nin, nout):
            return nn.Sequential(
                    nn.Linear(nin, nout),
                    nn.BatchNorm1d(nout),
                    nn.PReLU()
                )

        self.fc = nn.Sequential(
            *block(dim_in, hiddens[0]),
            *block(hiddens[0], hiddens[1]),
            *block(hiddens[1], hiddens[2]),
            nn.Linear(hiddens[2], dim_out),
            # nn.Softmax()
            nn.Sigmoid()
        )
    def forward(self, x):
        return self.fc(x)

class ResNet(nn.Module):
    def __init__(self, dim_in=61, dim_out=1):
        super(ResNet, self).__init__()

        hiddens = [128, 256, 32]
        self.res = nn.Sequential(
            BasicBlock3x3(1, 32),
            BasicBlock3x3(32, 32),
            BasicBlock3x3(32, 32),
            BasicBlock3x3(32, 32)
            )
        self.fc = nn.Sequential(
            nn.Linear(32*61, dim_out),
            # nn.Softmax()
            nn.Sigmoid()
        )

    def forward(self, x):
        x = x.unsqueeze(1)
        out = self.res(x)
        out = out.view(x.shape[0], 61*32)
        return self.fc(out)
