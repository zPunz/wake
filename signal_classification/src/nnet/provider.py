
from torch.utils.data import Dataset
import numpy as np
import torch

# Error: device-side assert triggered : labels contains -1, 5 > 4

class EEGDataset(Dataset):
    """Face Landmarks dataset."""

    def __init__(self, data, labels, transform=None, normalized=False):

        # if normalized:
        #     data = (data - np.mean(data))/np.std(data)
        data = torch.from_numpy(data)
        data = data.permute(0, 2, 1) #forehead

        self.signals = data
        self.labels = torch.from_numpy(labels)
        self.transform = transform

    def __len__(self):
        return len(self.labels)

    def __getitem__(self, idx):
        if torch.is_tensor(idx):
            idx = idx.tolist()

        signals = self.signals[idx, ...]
        labels = self.labels[idx]
        sample = (signals, labels)
        if self.transform:
            sample = self.transform(sample)

        return sample
