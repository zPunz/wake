
# -*- coding: utf-8 -*-
"""
    Created on Sat Mar 23 19:23:37 2019
    Edited on 12/07/2019
    @author: Tuan Dinh
"""

import os, csv
import numpy as np
import argparse
import warnings
warnings.filterwarnings("ignore")

parser = argparse.ArgumentParser()
parser.add_argument('--name', default='wakenet', type=str)
parser.add_argument('--data_path', default='../../data/', type=str)
parser.add_argument('--label_threshold', default=3, type=int)
parser.add_argument('--skip_threshold', default=1, type=int)
args = parser.parse_args()
# Data configuration
signal_types = ['EEG', 'EOGh', 'EOGv', 'EDA', 'EMG', 'EMG_active_detection', 'EMG_active_detection_2']
subjects = [9, 10, 12, 13, 15, 16, 19, 21, 22, 23, 24, 25, 26, 27, 28, 29, 30, 31, 33, 34, 35, 37, 39, 41]
subjects_w2sess = [21, 25, 27, 35, 38, 40, 41]
# Bad sections including the 'good one'
bad_sess_dict = {1: [11, 14, 16, 17, 18, 19, 20, 21, 26, 27, 29, 30, 36, 38, 40],
                2: [10, 11, 12, 14, 17, 18, 20, 21, 22, 25, 26, 27, 28, 29, 30, 31, 34, 36]}
# skip labels
bad_sess_dict_full = {1: [11, 14, 17, 18, 20, 36, 29, 38, 40], 2: [11, 14, 17, 18, 20, 36, 21, 27, 28]}

# no data: [38, 40]
# path
result_path = os.path.join(args.data_path, 'nnet/subject')
if not os.path.exists(result_path):
    os.makedirs(result_path)

class DataHelper:
    def __init__(self, window_size=5, frequency=200, label_threshold=3, skip_threshold=1):
        super(DataHelper, self).__init__()
        self.window_size = window_size
        self.frequency = frequency
        self.skip_threshold = skip_threshold
        self.label_threshold = label_threshold
        self.nsegments = None # doesn't matter for different subject
        self.nseconds = None

    def get_nseconds(self):
        return self.nseconds

    def set_length(self, eeg_size):
        self.nseconds = eeg_size // self.frequency # number of seconds
        self.nsegments = self.nseconds - self.window_size + 1 # number of segments, with stride 1s

    def segment(self, data):
        self.nseconds = data.shape[0] // self.frequency # number of seconds
        self.nsegments = self.nseconds - self.window_size + 1 # number of segments, with stride 1s
        # features
        segments = []
        for i in range(self.nsegments):
            start = i * self.frequency
            end = (i + self.window_size - 1) * self.frequency + 1
            s = data[start:end, :]
            segments.append(s)
        segments = np.stack(segments, axis=0)
        return segments

    def get_label(self, label_seconds, skip_seconds):
        labels = np.zeros(self.nsegments)
        for i in range(self.nsegments):
            s = i
            e = i + self.window_size - 1
            if np.sum(skip_seconds[s:e+1]) < self.skip_threshold and np.sum(label_seconds[s:e+1]) >= self.label_threshold:
                labels[i] = 1
        return labels


##  -- ops ---- ###
def load_data(subj, e_path):
    s_path = os.path.join(e_path, "filtered_WAKE_{}.csv".format(subj))
    if not(os.path.exists(s_path)):
        s_path = os.path.join(e_path, "Filtered_WAKE_{}.csv".format(subj))
    data = np.loadtxt(s_path, delimiter=',')
    if subj in ['EDA', 'EMG']:
        data = data[:, 1]
    return data

### Labeling ######
def read_csv(fpath):
    # get segments of positive labels
    print(fpath)
    pos_segs = []
    with open(fpath) as f:
        # read file eeg
        f_csv = csv.reader(f)
        headers = next(f_csv)
        for row in f_csv:
            pos_segs.append(row)
    return pos_segs

def get_label_second(pos_segs, nseconds):
    # number of segments:
    labels = np.zeros(nseconds)
    for row in range(len(pos_segs)):
        # time
        [m, s, ms] = pos_segs[row][0].split('.')
        start = int(m) * 60 + int(s) + (int(ms) + 5) // 10
        [m, s, ms] = pos_segs[row][1].split('.')
        end = int(m) * 60 + int(s) + (int(ms) + 5) // 10
        if end == nseconds:
            end = end - 1
        for i in range(start, end + 1):
            labels[i] = 1
    return labels

def load_label(e_path, nseconds, type='label'):
    if type == 'label':
        name = 'uSleep'
    elif type == 'skip':
        name = 'skipped'
    # labels
    label_path = os.path.join(e_path, "{}Episodes.csv".format(name))
    if not(os.path.exists(label_path)):
        label_path = os.path.join(e_path, "{}Epiosdes.csv".format(name))
    pos_segs = read_csv(label_path)
    label_seconds = get_label_second(pos_segs, nseconds)
    return label_seconds

def main():
    helper = DataHelper()
    for subj in subjects:
        X = []
        y = []
        nsessions = 2
        if subj in subjects_w2sess:
            nsessions = 1
        for session in range(1, 1+nsessions):
            print('== subject: {} session: {}'.format(subj, session))
            # skip bad session
            if subj in bad_sess_dict_full[session]:
                continue
            # path to e-signals
            e_path = os.path.join(args.data_path, "{:02d}/Session{}/ProcessedData/".format(
            subj, session))
            # data => get segments
            data = []
            for s in signal_types:
                d = load_data(s, e_path)
                if len(data) == 0:
                    # EEG
                    max_length = d.shape[0]
                    data.append(d[:, 0])
                    data.append(d[:, 1])
                else:
                    data.append(d[:max_length])
            # segment
            data = np.stack(data, axis=0)
            data = np.transpose(data)
            segments = helper.segment(data)
            # label
            nseconds = helper.get_nseconds()
            label_seconds = load_label(e_path, nseconds, type='label')
            skip_seconds = load_label(e_path, nseconds, type='skip')
            labels = helper.get_label(label_seconds, skip_seconds)
            # global vars
            X.append(segments)
            y.append(labels)
            # end of session
        # end of subj
        # combine all data
        X = np.concatenate(X, axis=0)
        y = np.concatenate(y, axis=0)
        # statistics
        print("---- data statistics ----")
        print(" == X shape: ", X.shape)
        print(" == #-segments: {} #-positive: {} #-negative: {}".format(X.shape[0], np.sum(y), X.shape[0] - np.sum(y)))
        # store
        savefile = os.path.join(result_path, "raw_segments_s{}.npy".format(subj))
        print("Save: ", savefile)
        np.save(savefile, {'X': X, 'y': y})

if __name__ == '__main__':
    main()
