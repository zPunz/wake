#!/usr/bin/env python
# coding: utf-8

import os
import numpy as np
import pandas as pd

from sklearn import preprocessing
from sklearn.model_selection import train_test_split
from sklearn.utils import resample

LAST_COL = 61

class Provider:

    @staticmethod
    def get_df(root):
        # read data into 1 file
        count = 0
        df = ''
        for file in sorted(os.listdir(root)):
            csv_path = os.path.join(root, file)
            df0 = pd.read_csv(csv_path, header=None)
            if 0 == count:
                df = df0
            else:
                df = df.append(df0)
            count += 1
        return df

    @staticmethod
    def upsample(df_train):
        # Separate majority and minority classes
        df_majority = df_train[df_train[LAST_COL]==0]
        df_minority = df_train[df_train[LAST_COL]==1]

        # Upsample minority class
        df_minority_upsampled = resample(df_minority,
                                         replace=True,     # sample with replacement
                                         n_samples=len(df_majority) // 2,    # to match majority class
                                         random_state=123) # reproducible results

        # Combine majority class with upsampled minority class
        df_upsampled = pd.concat([df_majority, df_minority_upsampled])
        return df_upsampled

    @staticmethod
    def downsample(df_train):
        # Separate majority and minority classes
        df_majority = df_train[df_train[LAST_COL]==0]
        df_minority = df_train[df_train[LAST_COL]==1]
        # Downsample majority class
        df_majority_downsampled = resample(df_majority,
                                         replace=False,    # sample without replacement
                                         n_samples=len(df_minority) * 2,     # to match minority class
                                         random_state=123) # reproducible results

        # Combine minority class with downsampled majority class
        df_downsampled = pd.concat([df_majority_downsampled, df_minority])
        return df_downsampled

    @staticmethod
    def load_data(up=True):
        # training
        df_train = Provider.get_df('train')
        x_train = df_train.values[:, :LAST_COL]
        # x_mean = np.mean(x_train, axis=0)
        # x_std = np.std(x_train, axis=0)
        x_min = np.min(x_train, axis=0)
        x_max = np.max(x_train, axis=0)

        df_test = Provider.get_df('test')
        if up:
            df_train = Provider.upsample(df_train)
        else:
            df_train = Provider.downsample(df_train)

        print('Train count: ', df_train[LAST_COL].value_counts())
        # standarization
        # data = df_upsampled.values
        data_train = df_train.values
        x_train = data_train[:, :LAST_COL]
        y_train = data_train[:, LAST_COL]
        # normalizing
        x_train = (x_train - x_min)/(x_max - x_min)

        # est
        data_test = df_test.values
        print('Test counts: ', df_test[LAST_COL].value_counts())
        x_test = data_test[:, :LAST_COL]
        y_test = data_test[:, LAST_COL]
        x_test = (x_test - x_min)/(x_max - x_min)

        return x_train, x_test, y_train, y_test
