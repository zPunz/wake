
"""
    @date: 12/14/2019
    @author: Tuan Dinh
    @email: dinh5@wisc.edu
    Define all neccessary helping methods
"""

import os
import pandas as pd
import numpy as np
from sklearn.metrics import precision_score, recall_score, accuracy_score
from sklearn.svm import SVC
from sklearn.ensemble import RandomForestClassifier, GradientBoostingClassifier, AdaBoostClassifier
from sklearn.linear_model import LogisticRegression
from sklearn.utils import resample
from sklearn.svm import LinearSVC
from sklearn.neighbors import KNeighborsClassifier
from mlens.ensemble import SuperLearner

class Helper:

    ####================= ops =============================#####
    @staticmethod
    def mkdir(name, rm=False):
        if not os.path.exists(name):
            os.makedirs(name)

    ####================= data =============================#####
    @staticmethod
    def upsample(df_train):
        # Separate majority and minority classes
        df_majority = df_train[df_train[LAST_COL]==0]
        df_minority = df_train[df_train[LAST_COL]==1]

        # Upsample minority class
        df_minority_upsampled = resample(df_minority,
                                         replace=False,     # sample with replacement
                                         n_samples=len(df_majority),    # to match majority class
                                         random_state=123) # reproducible results

        # Combine majority class with upsampled minority class
        df_upsampled = pd.concat([df_majority, df_minority_upsampled])
        return df_upsampled

    @staticmethod
    def downsample(df_majority, df_minority, sample_size):
        # Separate majority and minority classes
        downsample_factor = len(df_majority) // sample_size
        # Downsample majority class
        df_majority_downsampled = resample(df_majority,
                                         replace=False,    # sample without replacement
                                         n_samples=sample_size,     # to match minority class
                                         random_state=93) # reproducible results

        # Combine minority class with downsampled majority class
        df_downsampled = pd.concat([df_majority_downsampled, df_minority])
        return df_downsampled


    @staticmethod
    def divide_class(df):
        LAST_COL = df.shape[1] - 1
        df_minority = df[df[LAST_COL]==1]
        df_majority = df[df[LAST_COL]==0]
        return df_majority, df_minority


    @staticmethod
    def get_normalized(df, off=3):
        # feature : not the last 3: 2 events + labels
        d = df.values
        X = d[:, :-off]
        x_mean = np.mean(X, axis=0)
        x_std = np.std(X, axis=0)
        x_min = np.min(X, axis=0)
        x_max = np.max(X, axis=0)
        # X = (X - x_mean)/(x_max - x_min)
        X = (X - x_mean)/x_std
        d[:, :-off] = X
        return pd.DataFrame(data=d)

    @staticmethod
    def reject_outliers(df, off=3):
        def filter(data, m=10):
            return abs(data - np.median(data)) < m * np.std(data)
        # feature : not the last 3: 2 events + labels
        X = df.values
        for i in range(X.shape[1] - off):
            inds = filter(X[:, i])
            X = X[inds, :]
        return pd.DataFrame(data=X)


    @staticmethod
    def prepare_data(df, flag_downsample=False, flag_normalize=False):
        # read data into 1 file
        if flag_downsample:
            df_majority, df_minority = Helper.divide_class(df)
            sample_size = np.max([len(df_minority)])
            df = Helper.downsample(df_majority, df_minority, sample_size)
            downsample_factor = len(df_majority) // sample_size
            class_weight = dict({0:downsample_factor, 1:1.0})
        else:
            class_weight = dict({0:1.0, 1:1.5})

        # check #-positive
        if flag_normalize:
            df = Helper.get_normalized(df)

        LAST_COL = df.shape[1] - 1
        # data
        data = df.values
        np.random.shuffle(data)
        X = data[:, :LAST_COL]
        y = data[:, LAST_COL]
        return X, y, class_weight

    ####================= evaluate =============================#####
    @staticmethod
    def get_score(y_t, y_p):
        return [precision_score(y_t, y_p, labels=1), recall_score(y_t, y_p, labels=1)]



    ####================= classifier =============================#####
    """
        initialize simple classifier
        @params:
            class_weight: weights of 2 classes
            k [int]: classifier id
        @return classifier
    """
    @staticmethod
    def get_classifier(k, class_weight):
        # classifier
        if k == 0:
            # RandomForestClassifier
            classifier = RandomForestClassifier(n_estimators=20, class_weight=class_weight)
        elif k == 1:
            # SVM
            # classifier = SVC(kernel='linear')
            # classifier = SVC(kernel='poly', degree=8)
            classifier = SVC(kernel='rbf')
            # classifier = SVC(kernel='sigmoid')
        elif k ==2:
            classifier = LogisticRegression(random_state=0, class_weight=class_weight)
        elif k==3:
            classifier = GradientBoostingClassifier(random_state=0, learning_rate=0.1,
                          n_estimators=100)
        elif k==4:
            # svc=SVC(probability=True, kernel='linear')
            classifier = AdaBoostClassifier(n_estimators=50, random_state=0, learning_rate=0.1)
        else:
            # --- Build ---
            # Passing a scoring function will create cv scores during fitting
            # the scorer should be a simple function accepting to vectors and returning a scalar
            ensemble = SuperLearner(scorer=accuracy_score, verbose=2)

            # Build the first layer
            ensemble.add([RandomForestClassifier(n_estimators=100, class_weight=class_weight),
                            GradientBoostingClassifier(random_state=0, learning_rate=0.1,
                                          n_estimators=100),
                            SVC(kernel='rbf', class_weight=class_weight)])
            # Build the second layer
            ensemble.add([LogisticRegression()])

            classifier = ensemble

        return classifier

    @staticmethod
    def train_classifier(classifier, X_train, y_train, rule=False, lower_threshold=0.1):
        if rule:
            comp_inds = (X_train[:, -1] < lower_threshold) * (X_train[:, -2] < lower_threshold)
            inds = [not(e) for e in comp_inds]
            X_train = X_train[inds, :]
            y_train = y_train[inds]
            # predict
            classifier.fit(X_train, y_train)
        else:
            classifier.fit(X_train, y_train)

        return classifier

    @staticmethod
    def classify(classifier, X_test, rule=False, lower_threshold=0.1, proba=False):
        if rule:
            y_pred = classifier.predict(X_test)
            # correct
            inds = (X_test[:, -1] < lower_threshold)*(X_test[:, -2] < lower_threshold)
            y_pred[inds] = 0
        else:
            y_pred = classifier.predict(X_test)

        return y_pred
