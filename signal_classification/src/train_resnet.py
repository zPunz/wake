
"""
	@author Tuan Dinh tuandinh@cs.wisc.edu
	@date 08/14/2019
	Training 2 signals together
"""
import warnings
warnings.filterwarnings("ignore")
from _init_ import *
from model import Net

net = Net(61, 1).to(device)
net.apply(weights_init)
optimizer = optim.Adam(net.parameters(), lr=args.lr, betas=(0.5, 0.9))
scheduler = lr_scheduler.CyclicLR(optimizer, base_lr=args.lr, max_lr = 0.01, gamma=1.0, cycle_momentum=False)
criterion = nn.BCELoss()
# criterion = nn.CrossEntropyLoss(reduction='mean')

net_path = 'net.pth'

#training
start_time = time.time()
if os.path.isfile(net_path) and args.flag_retrain:
	print('Load existing models')
	net.load_state_dict(torch.load(net_path))

total_step = len(trainset) // args.batch_size
for epoch in range(args.num_epochs):
	lossfs = []
	for batch_idx, batch_data in enumerate(dataloader):
		data = batch_data.to(device)
		x = data[:, :-1]
		y = data[:, -1]#.type(torch.LongTensor).to(device)
		y_pred = net(x)
		optimizer.zero_grad()
		loss = criterion(y_pred, y)
		lossfs.append(loss.data.item())
		loss.backward()
		optimizer.step()

		if 0 == batch_idx % args.log_step:
			print('Epoch [{}/{}] Step [{}/{}] loss: {:.4f}'.format(epoch, args.num_epochs, batch_idx, total_step, np.mean(lossfs)))

	# train scores
	y_pred_train = net(x_train)
	y_pred_test = net(x_test)
	precision_train, recall_train = get_scores(y_train, y_pred_train, onecode=False)
	precision_test, recall_test = get_scores(y_test, y_pred_test, False)

	# evaluate
	writer.add_scalar('train/loss', np.mean(lossfs), epoch)
	writer.add_scalar('train/precision', precision_train, epoch)
	writer.add_scalar('train/recall', recall_train, epoch)
	writer.add_scalar('test/precision', precision_test, epoch)
	writer.add_scalar('test/recall', recall_test, epoch)

	print('[{}/{}] loss: {:.4f}, train {:.4f} {:.4f} test {:.4f} {:.4f}'.format(epoch, args.num_epochs, np.mean(lossfs), precision_train, recall_train, precision_test, recall_test))
	# decrease learning rate
	scheduler.step()
	## Saving and Sampling
	print('save model')
	torch.save(net.state_dict(), net_path)

writer.close()
