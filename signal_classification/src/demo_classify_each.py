"""
    @date: 12/14/2019
    @author: Tuan Dinh
    @email: dinh5@wisc.edu
    Classify microsleep segments for each single subject
"""

from _init_ import *

"""
    Perform classification task on a subject
    @params:
        df: data frame for the given subject
    @return scores
"""
def training_df(df):
    # load data
    X, y, class_weight = Helper.prepare_data(df, flag_downsample=args.flag_down, flag_normalize=args.flag_norm)
    # filter out sujects with small number of samples
    if np.sum(y) < LOW_POS_THRESHOLD:
        print("Length: ", np.sum(y))
        return -1
    # load the classifier
    classifier = Helper.get_classifier(args.classifier, class_weight)
    # switch the working mode
    if args.mode == 1:
        # LeaveOneOut
        y_t = []
        y_p = []
        loo = LeaveOneOut()
        loo.get_n_splits(X)
        for train_index, test_index in loo.split(X):
            X_train, X_test = X[train_index], X[test_index]
            y_train, y_test = y[train_index], y[test_index]
            y_pred = Helper.classify(classifier, X_train, y_train, X_test, args.flag_rule, args.event_threshold)
            y_t.append(y_test)
            y_p.append(y_pred)
        scores = Helper.get_score(y_t, y_p)
    elif args.mode == 0:
        # test-set
        X_train, X_test, y_train, y_test = train_test_split(X, y, stratify=y, test_size=args.test_size)
        y_pred = Helper.classify(classifier, X_train, y_train, X_test, args.flag_rule, args.event_threshold)
        scores = Helper.get_score(y_test, y_pred)
    elif args.mode == 2:
        kf = KFold(n_splits=args.nfolds)
        kf.get_n_splits(X)
        scores = []
        for train_index, test_index in kf.split(X):
            X_train, X_test = X[train_index], X[test_index]
            y_train, y_test = y[train_index], y[test_index]
            y_pred = Helper.classify(classifier, X_train, y_train, X_test, args.flag_rule, args.event_threshold)
            s = Helper.get_score(y_test, y_pred)
            scores.append(s)
        scores = scores/args.nfolds
    return scores

"""
    Perform classification task on all subjects, one by one
"""
def run():
    arr = []
    for file in sorted(os.listdir(args.data_path)):
        s = file.split('.')
        if not(s[1] == 'csv'):
            continue
        no = s[0].split('_')[-1]
        print('Evaluating ', no)
        csv_path = os.path.join(args.data_path, file)
        df = pd.read_csv(csv_path, header=None)
        row = training_df(df)
        if row == -1:
            continue
        arr.append([int(no)] + row)
    # result out
    for i in range(len(arr)):
        print(arr[i])
    arr = np.asarray(arr)
    save_path = os.path.join(result_path, 'each_{}_{}.csv'.format(mode_names[args.mode], classifier_names[args.classifier]))
    np.savetxt(save_path, arr, delimiter=',', fmt='%.2e', header='Subject, Precision, Recall')
    print('Done')

if __name__ == '__main__':
    print('Mode: ', mode_names[args.mode])
    print('Clasisifier: ', classifier_names[args.classifier])
    run()
