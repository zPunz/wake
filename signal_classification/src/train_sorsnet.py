
"""
	@date: 04/02/2020
	@author: Tuan Dinh
	@email: tuan.dinh@wisc.edu
"""
import os, sys, time
import pandas as pd
import numpy as np
import argparse
from sklearn import metrics

import torch
import torch.nn as nn
from torch.optim import lr_scheduler
import torch.optim as optim
from torch.utils.data import DataLoader
from torch.utils.tensorboard import SummaryWriter

from nnet.sorsnet import SorsNet
from nnet.helper import Helper, AverageMeter
from nnet.provider import EEGDataset
import warnings
warnings.filterwarnings("ignore")

parser = argparse.ArgumentParser()
parser.add_argument('--name', default='snet', type=str)
parser.add_argument('--data_dir', default='../data', type=str)
parser.add_argument('--checkpoint_dir', default='../results/checkpoints', type=str)

parser.add_argument('--num_subjects', default=7, type=int)
parser.add_argument('--num_channels', default=8, type=int)
parser.add_argument('--num_classes', default=2, type=int)
parser.add_argument('--num_linear_units', default=256, type=int)
parser.add_argument('--train_fraction', default=0.7, type=float)

parser.add_argument('--epochs', default=200, type=int)
parser.add_argument('--batch_size', default=64, type=int)
parser.add_argument('--lr', default=0.01, type=float)
parser.add_argument('--resume', default=0, type=int)
parser.add_argument('--is_test', action='store_true')

parser.add_argument('--log_steps', default=10, type=int)
parser.add_argument('--save_steps', default=30, type=int)
args = parser.parse_args()

writer = SummaryWriter('../results/runs/{}'.format(args.name))
device = torch.device("cuda:0")

# extarct eeg from multiple subjects
data = []
labels = []
subjects = [9, 10, 12, 13, 15, 16, 19, 21, 22, 23, 24, 25, 26, 27, 28, 29, 30, 31, 33, 34, 35, 37, 39, 41]
# subjects = [9]
missing_subjects = [26, 27, 29, 30, 16, 17]
for i in subjects:
	if i in missing_subjects:
		continue
	fpath = os.path.join(args.data_dir, 'nnet/subject/raw_segments_s{}.npy'.format(i))
	d = np.load(fpath, allow_pickle=True)
	d = d.item()
	data.append(d['X'])
	labels.append(d['y'])
data = np.concatenate(data, axis=0)
labels = np.concatenate(labels, axis=0)

# statistics
nsamples = data.shape[0]
args.num_channels = data.shape[2]
train_size = int(nsamples * args.train_fraction)
test_size = (nsamples - train_size)//2
val_size = nsamples - train_size - test_size
print('Dataset: {} Train {} Val {} Test {}'.format(nsamples, train_size, val_size, test_size))

# data
dataset = EEGDataset(data, labels, normalized=False)
train_set, val_set, test_set = torch.utils.data.random_split(dataset, [train_size, val_size, test_size])
train_loader = DataLoader(dataset=train_set,
    batch_size=args.batch_size, shuffle=True)
val_loader = DataLoader(dataset=val_set,
    batch_size=val_size, shuffle=False)
test_loader = DataLoader(dataset=test_set,
    batch_size=test_size, shuffle=False)

# model
# cnet = SleepNet(in_nc=args.num_channels, num_classes=args.num_classes).to(device)
cnet_path = os.path.join(args.checkpoint_dir, '{}.pth'.format(args.name))
cnet = SorsNet(args.num_channels, args.num_linear_units, args.num_classes).to(device)

# optimizer
optimizer = optim.Adam(cnet.parameters(), lr=args.lr, betas=(0.5, 0.9))
scheduler = lr_scheduler.StepLR(optimizer, step_size=60, gamma=0.2)
criterion = nn.CrossEntropyLoss(reduction='mean')

#training
if (args.resume > 0 or args.is_test) and os.path.isfile(cnet_path):
	print('Load existing models')
	cnet.load_state_dict(torch.load(cnet_path))

if not(args.is_test):
	elapsed_time = 0
	total_step = len(dataset) // args.batch_size
	for epoch in range(1, args.epochs + 1):
		start_time = time.time()
		cnet.train()
		losses = AverageMeter()
		top1 = AverageMeter()
		top2 = AverageMeter()
		# lr = Helper.learning_rate(args.lr, epoch - init_epoch, factor=40)
		# Helper.update_lr(optim_fnet, lr)
		lr = optimizer.param_groups[0]['lr']
		print('learning rate = %.7f' % lr)
		for batch_idx, (batch_data, labels) in enumerate(train_loader):
			inputs = batch_data.to(device, dtype=torch.float)
			labels = labels.to(device, dtype=torch.long)

			logits = cnet(inputs)
			loss = criterion(logits, labels)
			# measure accuracy and record loss
			prec1, prec2 = Helper.accuracy(logits, labels, topk=(1, 2))
			losses.update(loss.item(), inputs.size(0))
			top1.update(prec1.item(), inputs.size(0))
			top2.update(prec2.item(), inputs.size(0))

			optimizer.zero_grad()
			loss.backward()
			optimizer.step()

			if batch_idx % args.log_steps == 0:
				sys.stdout.write('\r')
				sys.stdout.write('| Epoch [%3d/%3d] Iter[%3d/%3d]\t\tLoss: %.4f Acc@1: %.3f Acc@2: %.3f'
							 % (epoch, args.epochs, batch_idx+1,
								total_step+1, loss.data.item(),
								top1.avg, top2.avg))
				sys.stdout.flush()

		# train scores
		# evaluate
		writer.add_scalar('train/loss', losses.avg, epoch)
		writer.add_scalar('train/acc', top1.avg, epoch)
		# writer.add_scalar('train/top2', top2.avg, epoch)

		scheduler.step()

		epoch_time = time.time() - start_time
		elapsed_time += epoch_time
		print('| Elapsed time : %d:%02d:%02d' % (Helper.get_hms(elapsed_time)))

		(batch_data, labels) = iter(val_loader).next()
		inputs = batch_data.to(device, dtype=torch.float)
		labels = labels.to(device)
		logits = cnet(inputs)
		prec1, prec2 = Helper.accuracy(logits, labels, topk=(1, 2))
		print("Evaluate on val-set: Acc1 {:.4f} Acc2 {:.4f}".format(prec1, prec2))

		writer.add_scalar('test/acc', prec1, epoch)

		if 0 == epoch % args.save_steps or epoch == args.epochs -1:
			## Saving and Sampling
			print('save model')
			torch.save(cnet.state_dict(), cnet_path)


writer.close()
# evaluate
(batch_data, labels) = iter(test_loader).next()
inputs = batch_data.to(device, dtype=torch.float)
labels = labels.to(device)
logits = cnet(inputs)
prec1, prec2 = Helper.accuracy(logits, labels, topk=(1, 2))
print("Evaluate on test-set: Acc1 {:.4f} Acc2 {:.4f}".format(prec1, prec2))
_, y_pred = torch.max(logits.data, 1)
y_pred = y_pred.cpu().numpy()
y_true = labels.cpu().numpy()
print("Scores: ", Helper.get_score(y_true, y_pred))
confusion = metrics.multilabel_confusion_matrix(y_true, y_pred)
print(confusion)
