import pdb
import torch
from torch.utils.data import Dataset
import numpy as np
import os
import random
import math
import pickle

def get_folders_path(data_root, test_subject_id):
    folders_train = []
    folders_test = []
    subject_id = [format(i,'02d') for i in range(9,42)]

    for i in range(9,42):
        subject_id = format(i,'02d')
        for j in range(10):
            session_id = 'Session'+str(j)
            folder_path = os.path.join(data_root,subject_id,session_id,'ProcessedData')
            if (i,j) not in [(21,2),(27,2),(29,1),(40,1),(38,1)] and os.path.isdir(folder_path):
                if i in test_subject_id:
                    folders_test.append(folder_path)
                else:
                    folders_train.append(folder_path)
    #folders_train.remove(data_root+'38/Session1/ProcessedData')
    #folders_train.remove(data_root+'40/Session1/ProcessedData')
    #folders_train.remove(data_root+'29/Session1/ProcessedData')
    #folders_train.remove(data_root+'27/Session2/ProcessedData')
    #folders_train.remove(data_root+'21/Session2/ProcessedData')
    return folders_train, folders_test

def load_csv(folder_path,device,signal_type,start_pos=None,end_pos=None):
    if os.path.isfile(os.path.join(folder_path,'filtered_'+device+'_'+signal_type+'.csv')):
        file_name = os.path.join(folder_path,'filtered_'+device+'_'+signal_type+'.csv')
    else:
        file_name = os.path.join(folder_path,'Filtered_'+device+'_'+signal_type+'.csv')


    # try:
    #     signal = np.genfromtxt(file_name,delimiter=',')
    # except TypeError:
    #     print(folder_path)
    signal = np.genfromtxt(file_name,delimiter=',')
    if start_pos is not None:
        signal = signal[start_pos:end_pos]
    return signal
    
def _read_files(folders,_sequence_len,_task,_target):
    len_per_file = []
    file_size = []
    for folder_path in folders:    
        signal = _target[folder_path]
        len_per_file.append(math.ceil(len(signal)/_sequence_len))
        file_size.append(len(signal))
    return len_per_file, file_size

class WAKEDATA(Dataset):
    def __init__(self,_data,_target,_len_per_file,_file_size,_sequence_len,_folders,start_channel=0,end_channel=1):
        self._data = _data
        self._target = _target
        self._len_per_file = _len_per_file
        self._file_size = _file_size
        self._sequence_len = _sequence_len
        self._folders = _folders
        self.start_channel = start_channel
        self.end_channel = end_channel
    def __getitem__(self,index):
        idx = index
        for i,l in enumerate(self._len_per_file):
            if idx>=l:
                idx = idx - l
            else:
                folder_path = self._folders[i]
                break
        start_pos = idx*self._sequence_len if idx<self._len_per_file[i]-1 else self._file_size[i]-self._sequence_len
        end_pos = start_pos + self._sequence_len
        target = self._target[folder_path][start_pos:end_pos,self.start_channel:self.end_channel]
        data = self._data[folder_path][start_pos:end_pos]
        return data,target
    def __len__(self):
        return int(np.sum(self._len_per_file))

class WAKEDATA_TEST(Dataset):
    def __init__(self,_data,_target,_folders,start_channel=0,end_channel=1):
        self._data = _data
        self._target = _target
        self._folders = _folders
        self.start_channel = start_channel
        self.end_channel = end_channel
    def __getitem__(self,index):
        return self._data[self._folders[index]], self._target[self._folders[index]][:,self.start_channel:self.end_channel]
    def __len__(self):
        return len(self._folders)
def get_data_sets(test_subject_id=[9],task='EOGh',channel=0,data_root='../WAKE-data/'):
    _folders_train, _folders_test = get_folders_path(data_root=data_root,test_subject_id=test_subject_id)
    _sequence_len = 50000

    _folders = _folders_train + _folders_test
    
    if os.path.isfile('preload/data.pkl'):
        print('Lucky, preload/data.pkl exists')
        _data = pickle.load(open('preload/data.pkl','rb'))
    else:
        _data = {}
        for folder_path in _folders:
            data = []
            for singal_type in ['alpha','beta','theta','EEG','EMG','EOGh','EOGv']:
                signal = load_csv(folder_path,'WAKE',singal_type)
                data.append(np.reshape(signal,[signal.shape[0],-1]))
            _data[folder_path] = np.concatenate(data,1)
        pickle.dump(_data,open('preload/data.pkl','wb'))
    
    if os.path.isfile('preload/target_%s.pkl'%(task)):
        print('Lucky, preload/target_%s.pkl exists'%(task))
        _target = pickle.load(open('preload/target_%s.pkl'%(task),'rb'))
    else:
        _target = {}
        for folder_path in _folders:
            _target[folder_path] = load_csv(folder_path,'PSG',task)
        pickle.dump(_target,open('preload/target_%s.pkl'%(task),'wb')) 

     
    _len_per_file_train, _file_size_train = _read_files(_folders_train,_sequence_len,task,_target)
    _len_per_file_test, _file_size_test = _read_files(_folders_test,_sequence_len,task,_target)

    channel_dict = {'alpha':(2,4),'theta':(0,2),'EOGh':(0,1),'EOGv':(0,1),'EEG':(0,4)}
    start_channel, end_channel = channel_dict[task]
    #start_channel, end_channel = start_channel, start_channel+1
    
    # indices = list(range(np.sum(_len_per_file)))
    # random.seed()
    # random.shuffle(indices)

    # train_indices = indices[:int(len(indices)*0.7)]
    # test_indices = indices[int(len(indices)*0.7):]

    train_dataset = WAKEDATA(_data,_target,_len_per_file_train,_file_size_train,_sequence_len,_folders_train,start_channel,end_channel)
    #test_dataset = WAKEDATA(_data,_target,_len_per_file_test,_file_size_test,_sequence_len,_folders_test)
    test_dataset = WAKEDATA_TEST(_data,_target,_folders_test,start_channel,end_channel)
    return train_dataset, test_dataset




