import torch
import torch.nn as nn
import torch.nn.functional as F 

import numpy as np
import pdb
from utils import *



class Model(nn.Module):
    def __init__(self,out_dim=None):
        super(Model, self).__init__()
        self.receptive_field = 1
        self.tuan = nn.Linear(34,0)
    def forward(self, input, target):
        input = input.transpose(1,2).contiguous()
        target = target.view([target.shape[0],target.shape[1],-1]).transpose(1,2).contiguous()
        pred = torch.cat((input[:,6:8:,:],input[:,6:8:,:]),1)
        #pred = input[:,4:6:,:]
        loss = None
        nor_crosscor = None
        if target is not None:
            loss = F.mse_loss(pred,target[:,:,-pred.shape[2]:],reduction='none')
            #N = pred.shape[2]
            #x = pred
            #y = target[:,:,-N:]
            #nor_crosscor, _len = normalized_cross_correlation(x,y,200)

        return loss, pred
