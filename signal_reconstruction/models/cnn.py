import torch
import torch.nn as nn
import torch.nn.functional as F 
from models.wavenet_modules import *

import numpy as np
import pdb




class Model(nn.Module):
    def __init__(self):
        super(Model, self).__init__()
        self._module = nn.Sequential(nn.Conv1d(1,1,kernel_size=300))
    
    def forward(self, input, target):
        input = input.transpose(1,2).contiguous()
        target = target.view([target.shape[0],target.shape[1],-1]).transpose(1,2).contiguous()
        pred = self._module(input[:,-1:,:])
        
        loss = None
        if target is not None:
            loss = F.mse_loss(pred,target[:,:,149:-150],reduction='mean')

        return loss, np.prod(pred.shape), pred

