import pdb
import torch
from torch.utils.data import Dataset
import numpy as np
import os
import random
import math
import pickle
from datetime import datetime

    
def read_files(files,seq_len,target):
    seq_per_file = []
    file_size = []
    for f in files:    
        signal = target[f]
        seq_per_file.append(math.ceil(len(signal)/seq_len))
        file_size.append(len(signal))
    return seq_per_file, file_size

class WAKEDATA(Dataset):
    def __init__(self,_data,_target,_len_per_file,_file_size,_sequence_len,_folders,start_channel=0,end_channel=1):
        self._data = _data
        self._target = _target
        self._len_per_file = _len_per_file
        self._file_size = _file_size
        self._sequence_len = _sequence_len
        self._folders = _folders
        self.start_channel = start_channel
        self.end_channel = end_channel
    def __getitem__(self,index):
        idx = index
        for i,l in enumerate(self._len_per_file):
            if idx>=l:
                idx = idx - l
            else:
                folder_path = self._folders[i]
                break
        start_pos = idx*self._sequence_len if idx<self._len_per_file[i]-1 else self._file_size[i]-self._sequence_len
        end_pos = start_pos + self._sequence_len
        target = self._target[folder_path][start_pos:end_pos,self.start_channel:self.end_channel]
        data = self._data[folder_path][start_pos:end_pos]
        return data,target
    def __len__(self):
        return int(np.sum(self._len_per_file))

class WAKEDATA_TEST(Dataset):
    def __init__(self,data,target,files):
        self.data = data
        self.target = target
        self.files = files
    def __getitem__(self,index):
        return self.data[self.files[index]], self.target[self.files[index]]
    def __len__(self):
        return len(self.files)



def get_data_sets(test_file='200506_PSG_Mask_Data.csv',task='EMG',data_root='../../MaskData/'):
    seq_len = 50000
    task_dict = {'EEG':0, 'EOG1':1, 'EOG2':2, 'EMG':3}
    target_channel = task_dict[task]

    files = ['200506_PSG_Mask_Data.csv','200507_PSG_Mask_Data.csv', '200508_PSG_Mask_Data.csv', '200512_PSG_Mask_Data.csv','200514_PSG_Mask_Data.csv', '200625_PSG_Mask_Data.csv']


    print(datetime.now())
    data = {}
    target = {}
    for f in files:
        signals = np.genfromtxt(os.path.join(data_root,f),delimiter=',')
        data[f] = signals[:,4:7]
        target[f] = signals[:,target_channel:target_channel+1]
    print(datetime.now())

    files.remove(test_file)
    train_files = files
    test_files = [test_file]
     

    seq_per_file_train, file_size_train = read_files(train_files,seq_len,target)

    train_dataset = WAKEDATA(data,target,seq_per_file_train,file_size_train,seq_len,train_files)
    #test_dataset = WAKEDATA(_data,_target,_len_per_file_test,_file_size_test,_sequence_len,_folders_test)
    test_dataset = WAKEDATA_TEST(data,target,test_files)
    return train_dataset, test_dataset




