import torch
from torch.utils.data import DataLoader
import torch.optim as optim
import torch.nn as nn
from torch.optim.lr_scheduler import ExponentialLR
from maskdataset import *

import importlib
import pdb
from datetime import datetime
from tqdm import tqdm
import pickle
from utils import *
import argparse
import os

parser = argparse.ArgumentParser()
parser.add_argument('--task', default='EMG', type=str, help='task.')
parser.add_argument('--test_file', default='200506_PSG_Mask_Data.csv', type=str, help='test file.')
parser.add_argument('--epochs', default=30, type=int, help='number of epochs')
args = parser.parse_args()

TASK = args.task
IN_CHANNELS=3
OUT_CHANNELS = 1
MODEL_NAME = 'wavenet'

NUM_EPOCH = args.epochs
BATCH_SIZE = 6

train_dataset, test_dataset = get_data_sets(args.test_file,TASK)

train_dataloader = DataLoader(train_dataset,batch_size=BATCH_SIZE,shuffle=True,num_workers=16)
test_dataloader = DataLoader(test_dataset,batch_size=1,shuffle=False,num_workers=16)

device_train = 'cuda:0'
device_test = 'cuda:0'
model_class = getattr(importlib.import_module('models.'+MODEL_NAME), 'Model')
model = model_class(in_channels=IN_CHANNELS,out_channels=OUT_CHANNELS)
#model = nn.DataParallel(model)
#model.to(device)
#model.load_state_dict(torch.load('wavenet.ckpt'))

optimizer = optim.Adam(model.parameters(),1e-3)
#optimizer = ExponentialLR(optimizer,0.98)

model_name = MODEL_NAME+'_mask_'+TASK+'_'+args.test_file
def train_epoch():
    model.to(device_train)
    model.train()
    total_loss = 0.0
    total_num_outputs = 0
    total_nor_crosscor = 0.0
    total_len = 0.0
    for x,y in tqdm(train_dataloader,total=len(train_dataloader),ncols=75,leave=False):
        x = x.type(torch.FloatTensor).to(device_train)
        y = y.type(torch.FloatTensor).to(device_train)
    
        optimizer.zero_grad()
        loss, pred = model(x,y)
        loss = loss.mean()
        loss.backward()
        optimizer.step()

        y = y.view([y.shape[0],y.shape[1],-1]).transpose(1,2).contiguous()
        target = y[:,:,-pred.shape[2]:]
        #nor_crosscor, _len = normalized_cross_correlation(pred,target,200)
        num_outputs = np.prod(pred.shape)
        total_loss += loss.to('cpu').item()*num_outputs
        #total_nor_crosscor += nor_crosscor.mean().to('cpu').item()*_len
        total_num_outputs += num_outputs
        #total_len += _len
        del loss
        del pred
    average_loss = total_loss/total_num_outputs
    #average_nor_crosscor = total_nor_crosscor/total_len
    return average_loss, 0

def test_epoch():
    model.to(device_test)
    model.eval()
    total_loss = 0.0
    total_num_outputs = 0
    total_nor_crosscor = 0.0
    total_nor_crosscor_power = 0.0
    total_len = 0.0
    total_len_power = 0.0
    with torch.no_grad():
        for x,y in tqdm(test_dataloader,total=len(test_dataloader),ncols=75,leave=False):
            x = x.type(torch.FloatTensor).to(device_test)
            y = y.type(torch.FloatTensor).to(device_test)
            
            loss = []
            pred = []
            for i in range(int((x.shape[1]+99999)/100000)):
                start = 0 if i==0 else i*100000-model.receptive_field+1
                end = (i+1)*100000
                x_segment = x[:,start:end,:]
                y_segment = y[:,start:end]
                loss_segment, pred_segment = model(x_segment,y_segment)
                pred.append(pred_segment.to('cpu').data.numpy())
                loss.append(loss_segment.to('cpu').data.numpy())
                del loss_segment
                del pred_segment
            loss = torch.from_numpy(np.concatenate(loss,2)).to(device_test)
            pred = torch.from_numpy(np.concatenate(pred,2)).to(device_test)
            y = y.view([y.shape[0],y.shape[1],-1]).transpose(1,2).contiguous()
            target = y[:,:,model.receptive_field-1:]
            nor_crosscor, _len = normalized_cross_correlation(pred,target,200)
            nor_crosscor_power, _len_power = normalized_cross_correlation(pred**2,target**2,200)
            loss = loss.mean()
            num_outputs = np.prod(pred.shape)
            total_loss += loss.to('cpu').item()*num_outputs
            total_nor_crosscor += nor_crosscor*_len
            total_nor_crosscor_power += nor_crosscor_power*_len_power
            total_num_outputs += num_outputs
            total_len += _len
            total_len_power += _len_power
        average_loss = total_loss/total_num_outputs
        average_nor_crosscor = total_nor_crosscor/total_len
        average_nor_crosscor_power = total_nor_crosscor_power/total_len_power
    return average_loss, average_nor_crosscor, average_nor_crosscor_power

best_test_loss = float('inf')
best_test_nor_crosscor = 0.0
best_epoch = -1
best_model_name = ''
best_mean_ncc = 0.0
for epoch in range(NUM_EPOCH):
    try:
        print('\nEpoch %d,    %s'%(epoch,best_model_name))
        train_loss, _ = train_epoch()
        print('Train RMSE: %.5f'%(train_loss))
        test_loss, test_nor_crosscor, test_nor_crosscor_power = test_epoch()
        mean_ncc = np.mean(test_nor_crosscor_power.to('cpu').data.numpy())
        if mean_ncc>best_mean_ncc:
            try:
                os.remove(best_model_name)
            except Exception:
                pass
            best_mean_ncc = mean_ncc
            best_test_loss = test_loss
            best_test_nor_crosscor = test_nor_crosscor
            best_test_nor_crosscor_power = test_nor_crosscor_power
            best_epoch = epoch
            best_model_name = 'saved/%s_%d_%.3f_%.3f.ckpt'%(model_name,best_epoch,best_test_loss,np.mean(test_nor_crosscor_power.to('cpu').data.numpy()))
            torch.save(model.state_dict(), best_model_name)
        print('Test Loss: %.5f,   Test Normalized Cross Corelation: '%(test_loss),test_nor_crosscor.to('cpu').data.numpy(), ' Test Normalized Cross Corelation Power: ',test_nor_crosscor_power.to('cpu').data.numpy())
    except KeyboardInterrupt:
        pdb.set_trace()

torch.save(model.state_dict(), 'saved/%s_last.ckpt'%(model_name))

f = open('{}.txt'.format(TASK),'a+')
print(best_model_name,file=f)





