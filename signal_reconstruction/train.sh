#!/bin/bash
for i in 9 10 12 13 15 16 19 21 22 23 24 25 26 27 28 29 30 31 33 34 35 37 38 39 40 41
do
  CUDA_VISIBLE_DEVICES=7 python3 -W ignore train.py --task EEG --test_subject_id $i --epochs 1
done
