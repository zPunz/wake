import torch
import pdb
def correlation(x,y):
    mean_x = torch.mean(x,2,keepdims=True)
    std_x = torch.std(x,2,keepdims=True)
    mean_y = torch.mean(y,2,keepdims=True)
    std_y = torch.std(y,2,keepdims=True)
    x = (x - mean_x)/std_x
    y = (y - mean_y)/std_y
    nor_crosscor = torch.mean(x*y,2)
    return nor_crosscor

def normalized_cross_correlation(x,y,max_shift=None):

    if max_shift == None or max_shift<=0:
        return correlation(x,y)
    else:
        _max = correlation(x,y)
        _len = torch.zeros_like(_max)+x.shape[2]
        for i in range(1,max_shift):
            x_i = x[:,:,:-i]
            y_i = y[:,:,i:]
            temp = correlation(x_i,y_i)
            c = temp>_max
            _max[c] = temp[c]
            _len.masked_fill_(c,x_i.shape[2])
            #if c.item():
            #    _max = temp
            #    _len = x_i.shape[2]
            
            x_i = x[:,:,i:]
            y_i = y[:,:,:-i]
            temp = correlation(x_i,y_i)
            c = temp>_max
            _max[c] = temp[c]
            _len.masked_fill_(c,x_i.shape[2])
            #if c.item():
            #    _max = temp
            #    _len = x_i.shape[2]


    return _max, _len

