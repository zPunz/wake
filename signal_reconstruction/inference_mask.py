import torch
from torch.utils.data import DataLoader
import torch.optim as optim
import torch.nn as nn
from torch.optim.lr_scheduler import ExponentialLR
from maskdataset import *

import importlib
import pdb
from datetime import datetime
from tqdm import tqdm
import pickle
import matplotlib as mpl
mpl.use('Agg')
import matplotlib.pyplot as plt
from scipy import signal
from scipy.fft import fftshift



TASK = 'EMG'
TEST_FILE = '200625_PSG_Mask_Data.csv'
OUT_CHANNELS = 1
IN_CHANNELS = 3
MODEL_NAME = 'wavenet'

NUM_EPOCH = 1000
BATCH_SIZE = 6

train_dataset, test_dataset = get_data_sets(TEST_FILE,TASK)

train_dataloader = DataLoader(train_dataset,batch_size=BATCH_SIZE,shuffle=True,num_workers=16)
test_dataloader = DataLoader(test_dataset,batch_size=1,shuffle=False,num_workers=16)

device_train = 'cuda:0'
device_test = 'cuda:0'
model_class = getattr(importlib.import_module('models.'+MODEL_NAME), 'Model')
model = model_class(in_channels=IN_CHANNELS,out_channels=OUT_CHANNELS)
#model = nn.DataParallel(model)
#model.to(device)
#model.load_state_dict(torch.load('wavenet.ckpt'))



model.load_state_dict(torch.load('saved/wavenet_mask_EMG_200625_PSG_Mask_Data.csv_15_160.278_0.136.ckpt'))
model.eval()

def test_epoch():
    model.to(device_test)
    model.eval()
    total_loss = 0.0
    total_num_outputs = 0
    total_nor_crosscor = 0.0
    total_len = 0.0
    for x,y in tqdm(test_dataloader,total=len(test_dataloader),ncols=75,leave=False):
        x = x.type(torch.FloatTensor).to(device_test)
        y = y.type(torch.FloatTensor).to(device_test)
        loss, pred, nor_crosscor, _len = model(x,y)
        loss = loss.mean()
        num_outputs = np.prod(pred.shape)
        total_loss += loss.to('cpu').item()*num_outputs
        total_nor_crosscor += nor_crosscor.mean().to('cpu').item()*_len
        total_num_outputs += num_outputs
        total_len += _len
    average_loss = np.sqrt(total_loss/total_num_outputs)
    average_nor_crosscor = total_nor_crosscor/total_len
    return average_loss, average_nor_crosscor

x, y = next(iter(test_dataloader))
model.to(device_test)
x = x.type(torch.FloatTensor).to(device_test)
y = y.type(torch.FloatTensor).to(device_test)
with torch.no_grad():
    loss, pred = model(x,y)

print('\n Start postprocessing and writing edf file')
pred = pred[0,0,:].to('cpu').data.numpy()
psg = y[0,:,0].to('cpu').data.numpy()
mask = x[0,:,2].to('cpu').data.numpy()
from utils_np import *
print(normalized_cross_correlation(pred**2,psg[-len(pred):]**2,200))
print(normalized_cross_correlation(mask**2,psg**2,200))

analytic_pred = signal.hilbert(pred)
pred_en = np.abs(analytic_pred)
analytic_mask = signal.hilbert(mask)
mask_en = np.abs(analytic_mask)
analytic_psg = signal.hilbert(psg)
psg_en = np.abs(analytic_psg)

print(normalized_cross_correlation(pred_en,psg_en[-len(pred):],200))
print(normalized_cross_correlation(mask_en,psg_en,200))

fs = 200
f_pred, t_pred, Sxx_pred = signal.spectrogram(pred, fs)
plt.pcolormesh(t_pred, f_pred, Sxx_pred, shading='gouraud')
plt.ylabel('Frequency [Hz]')
plt.xlabel('Time [sec]')
#plt.ylim(0,20)
plt.savefig('images/spectrogram_%s_%s_pred.png'%(TEST_FILE,TASK))
plt.close()

f_psg, t_psg, Sxx_psg = signal.spectrogram(psg[-len(pred):], fs)
plt.pcolormesh(t_psg, f_psg, Sxx_psg, shading='gouraud')
plt.ylabel('Frequency [Hz]')
plt.xlabel('Time [sec]')
#plt.ylim(0,20)
plt.savefig('images/spectrogram_%s_%s_psg.png'%(TEST_FILE,TASK))
plt.close()

f_mask, t_mask, Sxx_mask = signal.spectrogram(mask[-len(pred):], fs)
plt.pcolormesh(t_mask, f_mask, Sxx_mask, shading='gouraud')
plt.ylabel('Frequency [Hz]')
plt.xlabel('Time [sec]')
#plt.ylim(0,20)
plt.savefig('images/spectrogram_%s_%s_mask.png'%(TEST_FILE,TASK))
plt.close()

print(normalized_cross_correlation(np.reshape(Sxx_pred,[-1]),np.reshape(Sxx_psg,[-1]),0))
print(normalized_cross_correlation(np.reshape(Sxx_mask,[-1]),np.reshape(Sxx_psg,[-1]),0))



freqs_pred, psd_pred = signal.welch(pred)
plt.figure(figsize=(5, 4))
plt.semilogx(freqs_pred, psd_pred)
plt.title('PSD: power spectral density')
plt.xlabel('Frequency')
plt.ylabel('Power')
plt.tight_layout()
plt.savefig('images/psd_%s_%s_pred.png'%(TEST_FILE,TASK))
plt.close()


freqs_psg, psd_psg = signal.welch(psg)
plt.figure(figsize=(5, 4))
plt.semilogx(freqs_psg, psd_psg)
plt.title('PSD: power spectral density')
plt.xlabel('Frequency')
plt.ylabel('Power')
plt.tight_layout()
plt.savefig('images/psd_%s_%s_psg.png'%(TEST_FILE,TASK))
plt.close()


freqs_mask, psd_mask = signal.welch(mask)
plt.figure(figsize=(5, 4))
plt.semilogx(freqs_mask, psd_mask)
plt.title('PSD: power spectral density')
plt.xlabel('Frequency')
plt.ylabel('Power')
plt.tight_layout()
plt.savefig('images/psd_%s_%s_mask.png'%(TEST_FILE,TASK))
plt.close()


print(normalized_cross_correlation(psd_pred,psd_psg,0))
print(normalized_cross_correlation(psd_mask,psd_psg,0))
pdb.set_trace()

#from scipy import signal
#fs = 1000  # Sampling frequency
#fc = 30  # Cut-off frequency of the filter
#w = fc / (fs / 2) # Normalize the frequency
#b, a = signal.butter(5, w, 'low')
#pred = signal.filtfilt(b, a, pred)
#print('After low pass filter')
#print(normalized_cross_correlation(pred,psg[-len(pred):],200))
#pdb.set_trace()

padding = np.zeros(len(psg)-len(pred))
pred = np.concatenate([padding,pred],0)
import pyedflib
file_name = 'mics/s{}s1_{}.edf'.format(TEST_FILE,TASK)
f = pyedflib.EdfWriter(file_name,3)

#f.setSignalHeaders([{'label':'PSG','sample_rate':200},{'label':'MASK','sample_rate':200},{'label':'Wavenet Output','sample_rate':200}])
f.setLabel(0,'PSG')
f.setLabel(1,'BTE')
f.setLabel(2,'Wavenet Output')
f.setPhysicalDimension(0,'uV')
f.setPhysicalDimension(1,'uV')
f.setPhysicalDimension(2,'uV')
f.setSamplefrequency(0,200)
f.setSamplefrequency(1,200)
f.setSamplefrequency(2,200)
f.setPhysicalMaximum(0,max(psg))
f.setPhysicalMaximum(1,max(mask))
f.setPhysicalMaximum(2,max(pred))
f.setPhysicalMinimum(0,min(psg))
f.setPhysicalMinimum(1,min(mask))
f.setPhysicalMinimum(2,min(pred))

f.writeSamples([psg,mask,pred])

f.close()








#start = -30000
#end = start+3000
#
#mask = x[0,x.shape[1]+start:x.shape[1]+end,-2].to('cpu').data.numpy()
#
#psg = y[0,y.shape[1]+start:y.shape[1]+end].to('cpu').data.numpy()
#
#pred = pred[0,0,pred.shape[2]+start:pred.shape[2]+end].to('cpu').data.numpy()

#plt.plot(np.arange(end-start)/200, mask, markersize=1)
#plt.savefig('mask1.png')
#plt.close()
#
#
#plt.plot(np.arange(end-start)/200, psg, markersize=1)
#plt.savefig('psg1.png')
#plt.close()
#
#plt.plot(np.arange(end-start)/200, pred, markersize=1)
#plt.savefig('pred1.png')
#plt.close()
