import numpy as np
import pdb
def correlation(x,y):
    mean_x = np.mean(x,0,keepdims=True)
    std_x = np.std(x,0,keepdims=True)
    mean_y = np.mean(y,0,keepdims=True)
    std_y = np.std(y,0,keepdims=True)
    x = (x - mean_x)/std_x
    y = (y - mean_y)/std_y
    nor_crosscor = np.mean(x*y)
    return nor_crosscor

def normalized_cross_correlation(x,y,max_shift=None):

    if max_shift == None or max_shift<=0:
        return correlation(x,y)
    else:
        _max = correlation(x,y)
        _len = len(x)
        for i in range(1,max_shift):
            x_i = x[:-i]
            y_i = y[i:]
            temp = correlation(x_i,y_i)
            if temp>_max:
                _max = temp
                _len = len(x_i)
            
            x_i = x[i:]
            y_i = y[:-i]
            temp = correlation(x_i,y_i)
            if temp>_max:
                _max = temp
                _len = len(x_i)


    return _max

